<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
                    <h3 class="page-title">Dashboard</h3>
                    <div class="row">
                        <div class="col-lg-6">
                        <h4>User Information</h4>
                        <?php
                            echo form_open();
                            echo form_label('First name:', 'first_name') . '<br />';
                            echo form_error('first_name','<div class="alert alert-danger" role="alert">','</div>');
                            echo form_input('first_name', set_value('first_name'), array('class' => 'form-control')) . '<br />';
                            echo form_label('Last name:', 'last_name') . '<br />';
                            echo form_error('last_name','<div class="alert alert-danger" role="alert">','</div>');
                            echo form_input('last_name', set_value('last_name'), array('class' => 'form-control')) . '<br />';
                            echo form_label('Username:', 'username') . '<br />';
                            echo form_error('username','<div class="alert alert-danger" role="alert">','</div>');
                            echo form_input('username', set_value('username'), array('class' => 'form-control')) . '<br />';
                            echo form_label('Email:', 'email') . '<br />';
                            echo form_error('email','<div class="alert alert-danger" role="alert">','</div>');
                            echo form_input('email', set_value('email'), array('class' => 'form-control')) . '<br />';
                            echo form_label('Password:', 'password') . '<br />';
                            echo form_error('password','<div class="alert alert-danger" role="alert">','</div>');
                            echo form_password('password', set_value('password'), array('class' => 'form-control')) . '<br />';
                            echo form_label('Confirm password:', 'confirm_password') . '<br />';
                            echo form_error('confirm_password','<div class="alert alert-danger" role="alert">','</div>');
                            echo form_password('confirm_password', set_value('confirm_password'), array('class' => 'form-control')) . '<br /><br />';
                            echo form_submit('register','Register', array('class' => 'btn btn-sm btn-primary'));
                            echo form_close();
                        ?>
                        </div>
                        <div class="col-lg-6">
                            <h4>User Extras</h4>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris scelerisque lacinia eros in efficitur.
                                Phasellus quis elit nunc. Aliquam erat volutpat. Fusce sollicitudin nunc ac ipsum rutrum posuere.
                                Mauris non ex ac urna accumsan dignissim non nec elit. Donec volutpat tincidunt nulla, ac mollis
                                nisl mattis nec. Quisque et laoreet arcu. Aliquam dui lacus, hendrerit eu risus quis, lobortis
                                dictum tortor. Aliquam ac erat lobortis, pellentesque metus ac, facilisis sapien.
                            </p>
                        </div>
                    </div>
