<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Audit Log</h3>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="auditsListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#audits_filter').trigger("reset");
                    $.fn.loadAuditss();
                    $("#clearFilter").blur();
                });
                $("#doFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadAudits();
                    $("#doFilter").blur();
                });
                $.fn.loadAudits = function () {
                    $.blockUI({target: "#auditsListingTable"});
                    $params = $("#audits_filter").serialize();
                    $.get('<?php echo base_url('/audit/getAudits'); ?>?' + $params, function (data) {
                        $("#auditsListingTable").html(data);
                        $.unblockUI({target: "#auditsListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Audit Log!", "error");
                    });
                };
                $.fn.loadAudits();
            });
        </script>


