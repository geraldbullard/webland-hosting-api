<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
			<h3 class="page-title">Dashboard</h3>
			<?php
			if ($this->ion_auth->in_group('developer')) {
			?>
			<p><a href="https://linearicons.com/free" target="_blank">Linear Icons (Free)</a></p>
            <p><a href="https://fontawesome.com/icons?d=gallery&m=free" target="_blank">Font Awesome Icons (Free)</a></p>
            <p></p>
			<?php } ?>
            <div class="row">
                <div class="col-lg-12">
                    <?php RCI::get('dashboard', 'top', true); ?>
                    <p>&nbsp;</p>
                </div>
            </div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Weekly Overview</h3>
							<p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="metric" href="<?php echo base_url('dashboard/tracking') ?>">
										<span class="icon"><i class="fa fa-download"></i></span>
<!--										<p>-->
<!--											<span class="number">1,252</span>-->
<!--											<span class="title">Downloads</span>-->
<!--										</p>-->
                    <a class="btn btn-primary" href="<?php echo base_url('dashboard/tracking') ?>">Tracking</a>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-shopping-bag"></i></span>
										<p>
											<span class="number">203</span>
											<span class="title">Sales</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-eye"></i></span>
										<p>
											<span class="number">274,678</span>
											<span class="title">Visits</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										<p>
											<span class="number">35%</span>
											<span class="title">Conversions</span>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <?php
            if ($this->ion_auth->in_group('developer')) {
            ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Slider</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
                                    <input
                                        id="permission_slider"
                                        name="permission_slider"
                                        type="text"
                                        class="form-control permission-slider"
                                        data-slider-ticks="[0, 1, 2, 3, 4, 5]"
                                        data-slider-ticks-snap-bounds="30"
                                        data-slider-ticks-labels='["None", "View", "Add", "Edit", "Delete", "Global"]'
                                    />
                                    <script>
                                        $(".permission-slider").bootstrapSlider({
                                            ticks: [0, 1, 2, 3, 4, 5],
                                            ticks_labels: ['None', 'View', 'Add', 'Edit', 'Delete', 'Global'],
                                            ticks_snap_bounds: 30
                                        });
                                    </script>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row">
                <div class="col-lg-12">
                    <?php RCI::get('dashboard', 'afterslider', true); ?>
                    <p>&nbsp;</p>
                </div>
            </div>
			<div class="row">
				<div class="col-md-6">
					<!-- BUTTONS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Buttons</h3>
						</div>
						<div class="panel-body">
							<p class="demo-button">
								<button type="button" class="btn btn-default">Default</button>
								<button type="button" class="btn btn-primary">Primary</button>
								<button type="button" class="btn btn-info">Info</button>
								<button type="button" class="btn btn-success">Success</button>
								<button type="button" class="btn btn-warning">Warning</button>
								<button type="button" class="btn btn-danger">Danger</button>
							</p><br>

							<p class="demo-button">
								<button type="button" class="btn btn-primary btn-lg">Large Size</button>
								<button type="button" class="btn btn-primary">Default Size</button>
								<button type="button" class="btn btn-primary btn-sm">Small Size</button>
								<button type="button" class="btn btn-primary btn-xs">Extra Small Size</button>
							</p><br>

							<p class="demo-button">
								<button type="button" class="btn btn-default"><i class="fa fa-plus-square"></i> Default </button>
								<button type="button" class="btn btn-primary"><i class="fa fa-refresh"></i> Primary</button>
								<button type="button" class="btn btn-info"><i class="fa fa-info-circle"></i> Info</button>
								<button type="button" class="btn btn-primary" disabled="disabled"><i class="fa fa-refresh fa-spin"></i> Refreshing...</button>
							</p><br>

							<p class="demo-button">
								<button type="button" class="btn btn-success"><i class="fa fa-check-circle"></i> Success</button>
								<button type="button" class="btn btn-warning"><i class="fa fa-warning"></i> Warning</button>
								<button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Danger</button>
								<button type="button" class="btn btn-primary" disabled="disabled"><i class="fa fa-spinner fa-spin"></i> Loading...</button>
							</p><br>

							<div class="row">
								<div class="col-md-6"><button type="button" class="btn btn-primary btn-block">Button Block</button></div>
								<div class="col-md-6"><button type="button" class="btn btn-warning btn-block">Button Block</button></div>
							</div>
						</div>
					</div>
					<!-- END BUTTONS -->

					<!-- INPUTS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Inputs</h3>
						</div>
						<div class="panel-body">
							<input type="text" class="form-control" placeholder="text field"><br>
							<input type="password" class="form-control" value="asecret"><br>
							<textarea class="form-control" placeholder="textarea" rows="4"></textarea><br>
							<select class="form-control">
								<option value="cheese">Cheese</option>
								<option value="tomatoes">Tomatoes</option>
								<option value="mozarella">Mozzarella</option>
								<option value="mushrooms">Mushrooms</option>
								<option value="pepperoni">Pepperoni</option>
								<option value="onions">Onions</option>
							</select><br>

							<label class="fancy-checkbox">
								<input type="checkbox">
								<span>Fancy Checkbox 1</span>
							</label>
							<label class="fancy-checkbox">
								<input type="checkbox">
								<span>Fancy Checkbox 2</span>
							</label>
							<label class="fancy-checkbox">
								<input type="checkbox">
								<span>Fancy Checkbox 3</span>
							</label><br>

							<label class="fancy-radio">
								<input name="gender" value="male" type="radio">
								<span><i></i>Male</span>
							</label>
							<label class="fancy-radio">
								<input name="gender" value="female" type="radio">
								<span><i></i>Female</span>
							</label>
						</div>
					</div>
					<!-- END INPUTS -->

					<!-- INPUT SIZING -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Input Sizing</h3>
						</div>
						<div class="panel-body">
							<input class="form-control input-lg" placeholder=".input-lg" type="text"><br>
							<input class="form-control" placeholder="Default input" type="text"><br>
							<input class="form-control input-sm" placeholder=".input-sm" type="text"><br>
							<select class="form-control input-lg">
								<option value="cheese">Cheese</option>
								<option value="tomatoes">Tomatoes</option>
								<option value="mozarella">Mozzarella</option>
								<option value="mushrooms">Mushrooms</option>
								<option value="pepperoni">Pepperoni</option>
								<option value="onions">Onions</option>
							</select><br>
							<select class="form-control">
								<option value="cheese">Cheese</option>
								<option value="tomatoes">Tomatoes</option>
								<option value="mozarella">Mozzarella</option>
								<option value="mushrooms">Mushrooms</option>
								<option value="pepperoni">Pepperoni</option>
								<option value="onions">Onions</option>
							</select><br>
							<select class="form-control input-sm">
								<option value="cheese">Cheese</option>
								<option value="tomatoes">Tomatoes</option>
								<option value="mozarella">Mozzarella</option>
								<option value="mushrooms">Mushrooms</option>
								<option value="pepperoni">Pepperoni</option>
								<option value="onions">Onions</option>
							</select>
						</div>
					</div>
					<!-- END INPUT SIZING -->
				</div>
				<div class="col-md-6">
					<!-- LABELS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Labels &amp; Badges</h3>
						</div>
						<div class="panel-body">
							<span class="label label-default">DEFAULT</span>
							<span class="label label-primary">PRIMARY</span>
							<span class="label label-success">SUCCESS</span>
							<span class="label label-info">INFO</span>
							<span class="label label-warning">WARNING</span>
							<span class="label label-danger">DANGER</span>

							<br><br>

							<a href="#">Inbox <span class="badge">42</span></a>

							<br><br>

							<button class="btn btn-primary" type="button">
								Messages <span class="badge">4</span>
							</button><br /><br />
						</div>
					</div>
					<!-- END LABELS -->

					<!-- PROGRESS BARS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Progress Bars</h3>
						</div>
						<div class="panel-body">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
									<span class="sr-only">60% Complete (warning)</span>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
									<span class="sr-only">60% Complete (success)</span>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
									<span class="sr-only">60% Complete (danger)</span>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
									75%
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
									<span class="sr-only">20% Complete</span>
								</div>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
									<span class="sr-only">45% Complete</span>
								</div>
							</div>
							<div class="progress progress-xs">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
									<span class="sr-only">80% Complete</span>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROGRESS BARS -->

					<!-- INPUT GROUPS -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Input Groups</h3>
						</div>
						<div class="panel-body">
							<div class="input-group">
								<input class="form-control" type="text">
								<span class="input-group-btn"><button class="btn btn-primary" type="button">Go!</button></span>
							</div><br>
							<div class="input-group">
									<span class="input-group-btn">
										<button class="btn btn-primary" type="button">Go!</button>
									</span>
								<input class="form-control" type="text">
							</div><br>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input class="form-control" placeholder="Username" type="text">
							</div><br>
							<div class="input-group">
								<input class="form-control" placeholder="Username" type="text">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
							</div><br>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input class="form-control" type="text">
								<span class="input-group-addon">.00</span>
							</div>
						</div>
					</div>
					<!-- END INPUT GROUPS -->

					<!-- ALERT MESSAGES -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Alert Messages</h3>
						</div>
						<div class="panel-body">
							<div class="alert alert-info alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								<i class="fa fa-info-circle"></i> The system is running well
							</div>
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								<i class="fa fa-check-circle"></i> Your settings have been succesfully saved
							</div>
							<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								<i class="fa fa-warning"></i> Warning, check your permission settings
							</div>
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
								<i class="fa fa-times-circle"></i> Your account has been suspended
							</div>
						</div>
					</div>
					<!-- END ALERT MESSAGES -->
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">To-Do List</h3>
							<div class="right">
								<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
							</div>
						</div>
						<div class="panel-body">
							<ul class="list-unstyled todo-list">
								<li>
									<label class="control-inline fancy-checkbox">
										<input type="checkbox"><span></span>
									</label>
									<p>
										<span class="title">Restart Server</span>
										<span class="short-description">Dynamically integrate client-centric technologies without cooperative resources.</span>
										<span class="date">Oct 9, 2016</span>
									</p>
									<div class="controls">
										<a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
									</div>
								</li>
								<li>
									<label class="control-inline fancy-checkbox">
										<input type="checkbox"><span></span>
									</label>
									<p>
										<span class="title">Retest Upload Scenario</span>
										<span class="short-description">Compellingly implement clicks-and-mortar relationships without highly efficient metrics.</span>
										<span class="date">Oct 23, 2016</span>
									</p>
									<div class="controls">
										<a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
									</div>
								</li>
								<li>
									<label class="control-inline fancy-checkbox">
										<input type="checkbox"><span></span>
									</label>
									<p>
										<strong>Functional Spec Meeting</strong>
										<span class="short-description">Monotonectally formulate client-focused core competencies after parallel web-readiness.</span>
										<span class="date">Oct 11, 2016</span>
									</p>
									<div class="controls">
										<a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">My Tasks</h3>
							<div class="right">
								<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
							</div>
						</div>
						<div class="panel-body" style="display: block;">
							<ul class="list-unstyled task-list">
								<li>
									<p>Updating Users Settings <span class="label-percent">23%</span></p>
									<div class="progress progress-xs">
										<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width:23%">
											<span class="sr-only">23% Complete</span>
										</div>
									</div>
								</li>
								<li>
									<p>Load &amp; Stress Test <span class="label-percent">80%</span></p>
									<div class="progress progress-xs">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
											<span class="sr-only">80% Complete</span>
										</div>
									</div>
								</li>
								<li>
									<p>Data Duplication Check <span class="label-percent">100%</span></p>
									<div class="progress progress-xs">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
											<span class="sr-only">Success</span>
										</div>
									</div>
								</li>
								<li>
									<p>Server Check <span class="label-percent">45%</span></p>
									<div class="progress progress-xs">
										<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
											<span class="sr-only">45% Complete</span>
										</div>
									</div>
								</li>
								<li>
									<p>Mobile App Development <span class="label-percent">10%</span></p>
									<div class="progress progress-xs">
										<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
											<span class="sr-only">10% Complete</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div id="toastr-demo" class="panel">
						<div class="panel-body">
							<!-- CONTEXTUAL -->
							<h4>Context</h4>
							<p class="demo-button">
								<button type="button" class="btn btn-primary btn-toastr" data-context="info" data-message="This is general theme info" data-position="top-right">General Info</button>
								<button type="button" class="btn btn-success btn-toastr" data-context="success" data-message="This is success info" data-position="top-right">Success Info</button>
								<button type="button" class="btn btn-warning btn-toastr" data-context="warning" data-message="This is warning info" data-position="top-right">Warning Info</button>
								<button type="button" class="btn btn-danger btn-toastr" data-context="error" data-message="This is error info" data-position="top-right">Error Info</button>
							</p>
							<!-- END CONTEXTUAL -->

							<br>

							<!-- POSITION -->
							<h4>Position</h4>
							<p class="demo-button">
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="bottom-right">Bottom Right</button>
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="bottom-left">Bottom Left</button>
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="top-left">Top Left</button>
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="top-right">Top Right</button>
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="top-full-width">Top Full Width</button>
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="bottom-full-width">Bottom Full Width</button>
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="top-center">Top Center</button>
								<button type="button" class="btn btn-default btn-toastr" data-context="info" data-message="Hi, I'm here" data-position="bottom-center">Bottom Center</button>
							</p>
							<!-- END POSITION -->

							<br>

							<!-- CALLBACK -->
							<h4>Callback</h4>
							<p class="demo-button">
								<button type="button" class="btn btn-default btn-toastr-callback" id="toastr-callback1" data-context="info" data-message="onShown and onHidden callback demo">onShown and onHidden</button>
								<button type="button" class="btn btn-default btn-toastr-callback" id="toastr-callback2" data-context="info" data-message="Please click me">onclick</button>
								<button type="button" class="btn btn-default btn-toastr-callback" id="toastr-callback3" data-context="info" data-message="Please click close button">onCloseClick</button>
							</p>
							<!-- END CALLBACK -->
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-headline">
						<div class="panel-body">
							<h1>Dashboard Heading 1</h1>
							<h2>Dashboard Heading 2</h2>
							<h3>Dashboard Heading 3</h3>
							<h4>Dashboard Heading 4</h4>
							<h5>Dashboard Heading 5</h5>
							<h6>Dashboard Heading 6</h6>

							<hr>

							<p>Appropriately benchmark web-enabled bandwidth and functionalized leadership skills. Conveniently syndicate global opportunities without interactive methods of empowerment. Collaboratively conceptualize user-centric e-tailers for visionary methodologies. Dramatically myocardinate. Phosfluorescently disintermediate unique resources whereas reliable mindshare. Competently optimize client-focused infrastructures vis-a-vis e-business human capital. Uniquely formulate sustainable benefits whereas functional results. Energistically myocardinate bleeding-edge e-business.</p>

							<hr>

							<p class="text-muted"><code>.text-muted</code> Convey meaning through color with a handful of emphasis utility classes.</p>
							<p class="text-primary"><code>.text-primary</code> Convey meaning through color with a handful of emphasis utility classes.</p>
							<p class="text-success"><code>.text-success</code> Convey meaning through color with a handful of emphasis utility classes.</p>
							<p class="text-info"><code>.text-info</code> Convey meaning through color with a handful of emphasis utility classes.</p>
							<p class="text-warning"><code>.text-warning</code> Convey meaning through color with a handful of emphasis utility classes.</p>
							<p class="text-danger"><code>.text-danger</code> Convey meaning through color with a handful of emphasis utility classes.</p>

							<hr>

							<p>Make a paragraph stand out by adding <code>.lead</code></p>
							<p class="lead">Objectively re-engineer maintainable total linkage after proactive intellectual capital. Dynamically evolve best-of-breed e-services for user-centric customer.</p>

							<hr>

							<div class="well">
								<p class="text-left"><code>.text-left</code> Left aligned text.</p>
								<p class="text-center"><code>.text-center</code> Center aligned text.</p>
								<p class="text-right"><code>.text-right</code> Right aligned text.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
            <?php } ?>
			<div class="row">
				<div class="col-lg-12">
					<?php RCI::get('dashboard', 'bottom', true); ?>
                    <p>&nbsp;</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">&nbsp;</div>
			</div>