<?php
/** Name: Default */
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-heading"><?php echo $pagedata->title; ?></h1>
            <?php echo $pagedata->content; ?>
        </div>
    </div>
</div>
