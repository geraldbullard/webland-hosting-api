<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/users">Users</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="panel panel-profile">
			<div class="clearfix">
				<!-- LEFT COLUMN -->
				<div class="profile-left">
					<!-- PROFILE HEADER -->
					<div class="profile-header">
						<div class="overlay"></div>
						<div class="profile-main">
							<img src="<?php echo base_url('assets/klorofil/img/user-medium.png'); ?>" class="img-circle" alt="Avatar">
							<h3 class="name">
								<?php echo $user[0]['first_name']; ?> <?php echo $user[0]['last_name']; ?>
							</h3>
							<span class="online-status status-<?php
								echo ($user[0]['active'] == 1 ? 'available' : 'unavailable');
							?>"><?php
								echo ($user[0]['active'] == 1 ? 'Active' : 'Inactive');
							?></span>
						</div>
						<div class="profile-stat">
							<div class="row">
								<div class="col-md-4 stat-item">
									45 <span>Stat #1</span>
								</div>
								<div class="col-md-4 stat-item">
									15 <span>Stat #2</span>
								</div>
								<div class="col-md-4 stat-item">
									2174 <span>Stat #3</span>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE HEADER -->
					<!-- PROFILE DETAIL -->
					<div class="profile-detail">
						<div class="profile-info">
							<h4 class="heading">User Information</h4>
							<ul class="list-unstyled list-justify">
								<li>Date Created <span><?php
									echo date("m-d-Y h:i A", $user[0]['created_on']);
								?></span></li>
								<li>Last Login <span><?php
									echo date("m-d-Y h:i A", $user[0]['last_login']);
								?></span></li>
								<li>Status <span><?php
									echo ($user[0]['active'] > 0 ? 'Active' : 'Inactive');
								?></span></li>
								<li>Activity <span>See All</span></li>
							</ul>
                            <hr />
                            <?php if ($this->session->flashdata('msg')) { ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> action-alert" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                                            <?php echo $this->session->flashdata('msg'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
							<?php echo form_open_multipart('upload/do_upload'); ?>
                            <input type="hidden" name="userid" value="<?php echo $user[0]['id']; ?>" />
                            <input type="hidden" name="redirect" value="/users/edit/<?php echo $user[0]['id']; ?>" />
                            <h4 class="heading">User Image</h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input class="form-control" type="file" name="userfile" size="20" />
                                    </div><br /><br />
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-block">Upload New Image</button>
                                    </div>
                                </div>
							</form>
						</div>
					</div>
					<!-- END PROFILE DETAIL -->
				</div>
				<!-- END LEFT COLUMN -->
				<!-- RIGHT COLUMN -->
				<div class="profile-right">
					<h4>Details</h4>
					<form action="" method="post" id="user_edit">
						<p>
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name" value="<?php echo $user[0]['first_name']; ?>">
                        </p>
                        <p>
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" name="last_name" value="<?php echo $user[0]['last_name']; ?>">
                        </p>
                        <p>
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" value="<?php echo $user[0]['username']; ?>">
                        </p>
                        <p>
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" value="<?php echo $user[0]['email']; ?>">
                        </p>
                        <p>
                            <label for="company">Company</label>
                            <input type="text" class="form-control" name="company" value="<?php echo $user[0]['company']; ?>">
                        </p>
                        <p>
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" value="<?php echo $user[0]['phone']; ?>">
                        </p>
                        <p>
                            <label for="password">Password <small>(Leave empty to keep current password)</small></label>
                            <input type="password" class="form-control" id="password" name="password" value="">
                        </p>
                        <p>
                            <label for="passconf">Password Confirm</label>
                            <input type="password" class="form-control" id="passconf" name="passconf" value="">
                        </p>
						<p>
							<?php
								$enabled = ' disabled';
								if (
									(
										$this->ion_auth_model->in_group("developer", $user[0]['id']) &&
										$this->ion_auth_model->in_group("developer", $this->session->userdata('user_id'))
									) || !$this->ion_auth_model->in_group("developer", $user[0]['id'])
								) {
									$enabled = '';
								}
							?>
                            <label for="groups">User Groups</label>
							<select class="form-control" id="groups" name="groups[]" multiple<?php echo $enabled; ?>>
                                <?php
                                foreach ($this->ion_auth->groups()->result() as $group) {
                                    echo '<option value="' . $group->id . '"';
                                    echo ($this->ion_auth->in_group($group->id, $user[0]['id']) > 0 ? ' selected' : null);
                                    echo '>' . $group->description;
                                    echo '</option>';
                                }
                                ?>
							</select>
						</p>
                        <p class="text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </p>
					</form>
				</div>
				<!-- END RIGHT COLUMN -->
			</div>
		</div>
