<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->template->title->default("Default title"); ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link rel="icon" href="/assets/estartup/img/favicon.png">
    <link rel="apple-touch-icon" href="/assets/estartup/img/apple-touch-icon.png">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link rel="stylesheet" href="/assets/estartup/lib/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Libraries CSS Files -->
    <link rel="stylesheet" href="/assets/estartup/lib/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/estartup/lib/owlcarousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="/assets/estartup/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/fa-all-5.13.0.css">
    <link rel="stylesheet" href="/assets/estartup/lib/animate/animate.min.css">
    <link rel="stylesheet" href="/assets/estartup/lib/modal-video/css/modal-video.min.css">
    <!-- Main Stylesheet File -->
    <link rel="stylesheet" href="/assets/estartup/css/style.css">
    <link rel="stylesheet" href="/assets/estartup/css/bp_custom.css">
    <?php echo $this->template->stylesheet; ?>
    <!-- =======================================================
       Theme Name: eStartup
       Theme URL: https://bootstrapmade.com/estartup-bootstrap-landing-page-template/
       Author: BootstrapMade.com
       License: https://bootstrapmade.com/license/
       ======================================================= -->
</head>
<body>
<header id="header" class="header header-hide">
    <div class="container">
        <div id="logo" class="pull-left">
            <h1><a href="#body" class="scrollto"><span>e</span>Startup</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
        </div>
        <?php echo $this->template->widget('estartup/primarynav'); ?>
    </div>
</header>
<!-- #header -->
<?php echo $this->template->content; ?>
<!--==========================
   Footer
   ============================-->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <div class="footer-logo">
                    <a class="navbar-brand" href="#">eStartup</a>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the.
                    </p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2">
                <div class="list-menu">
                    <h4>Abou Us</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Features item</a></li>
                        <li><a href="#">Live streaming</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2">
                <div class="list-menu">
                    <h4>Abou Us</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Features item</a></li>
                        <li><a href="#">Live streaming</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2">
                <div class="list-menu">
                    <h4>Support</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">faq</a></li>
                        <li><a href="#">Editor help</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 col-lg-2">
                <div class="list-menu">
                    <h4>Abou Us</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Features item</a></li>
                        <li><a href="#">Live streaming</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyrights">
        <div class="container">
            <p>&copy; Copyright brokenPIXEL. All rights reserved.</p>
            <div class="credits">
                <!--
                   All the links in the footer should remain intact.
                   You can delete the links only if you purchased the pro version.
                   Licensing information: https://bootstrapmade.com/license/
                   Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eStartup
                   -->
                Theme Design by <a target="_blank" href="https://bootstrapmade.com/">BootstrapMade</a><br>
                Distributed for free by <a target="_blank" href="https://themewagon.com/">ThemeWagon</a><br>
                Modified for <a target="_blank" href="https://brokenpixelcms.com/">brokenPIXEL</a> by <a href="javascript:;">ForgottenStudios</a>
            </div>
        </div>
    </div>
</footer>
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<!-- JavaScript Libraries -->
<script src="/assets/estartup/lib/jquery/jquery.min.js"></script>
<script src="/assets/estartup/lib/jquery/jquery-migrate.min.js"></script>
<script src="/assets/estartup/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/estartup/lib/superfish/hoverIntent.js"></script>
<script src="/assets/estartup/lib/superfish/superfish.min.js"></script>
<script src="/assets/estartup/lib/easing/easing.min.js"></script>
<script src="/assets/estartup/lib/modal-video/js/modal-video.js"></script>
<script src="/assets/estartup/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/assets/estartup/lib/wow/wow.min.js"></script>
<!-- Template Main Javascript File -->
<script src="/assets/estartup/js/main.js"></script>
<?php echo $this->template->javascript; ?>
</body>
</html>