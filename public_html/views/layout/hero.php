<div class="hero-unit">
	<h1>Welcome to BonFire4!</h1>
	<p>
        We hope you love the new design, features and upgraded responsiveness of the all new BonFire4 platform.
    </p>
	<p>
        <a class="btn btn-primary btn-large" href="/user/login/">Login Now</a>
    </p>
</div>