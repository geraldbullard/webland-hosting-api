<!-- Customize these classes for say a footer nav etc -->
<nav class="nav-menu d-none d-lg-block">
    <?php
        $this->load->helper('contents/functions');
        $tree = unserialize($menu->menuData);
        $treeMenu = parseTree($tree['item'], 0);
        $newTreeMenu = mergeExtrasIntoTree($treeMenu, $tree);
        printPlainMenuTree($newTreeMenu);
    ?>
</nav>
