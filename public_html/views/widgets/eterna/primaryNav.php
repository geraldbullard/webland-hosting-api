<nav class="nav-menu d-none d-lg-block">
    <?php
        /*
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href="index.html">Home</a></li>
                <li class="drop-down"><a href="#">About</a>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="team.html">Team</a></li>
                        <li class="drop-down"><a href="#">Drop Down 2</a>
                            <ul>
                                <li><a href="#">Deep Drop Down 1</a></li>
                                <li><a href="#">Deep Drop Down 2</a></li>
                                <li><a href="#">Deep Drop Down 3</a></li>
                                <li><a href="#">Deep Drop Down 4</a></li>
                                <li><a href="#">Deep Drop Down 5</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="services.html">Services</a></li>
                <li><a href="portfolio.html">Portfolio</a></li>
                <li><a href="pricing.html">Pricing</a></li>
                <li><a href="blog.html">Blog</a></li>
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </nav>    
        */
        // Set theme specific options if needed
        $options = array(
            'ulClass' => '',
            'ulStyle' => '',
            'liClass' => '',
            'dropClass' => 'drop-down',
            'dropHrefClass' => '',
        );
        $this->load->helper('contents/functions');
        $tree = unserialize($menu->menuData);
        $treeMenu = parseTree($tree['item'], 0);
        $newTreeMenu = mergeExtrasIntoTree($treeMenu, $tree);
        printPlainMenuTree($newTreeMenu, $options);
    ?>
</nav>
