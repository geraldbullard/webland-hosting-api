<!-- ======= Top Bar ======= -->
<section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
        <div class="contact-info mr-auto">
            <i class="icofont-envelope"></i><a href="mailto:<?php echo $items['email']; ?>"><?php echo $items['email']; ?></a>
            <i class="icofont-phone"></i><a href="tel:<?php
                echo preg_replace('/[^0-9]/', '', $items['phone']); ?>"><?php echo $items['phone']; ?></a>
        </div>
        <div class="social-links">
            <a target="_blank" href="<?php echo $items['twitter']; ?>" class="twitter"><i class="icofont-twitter"></i></a>
            <a target="_blank" href="<?php echo $items['facebook']; ?>" class="facebook"><i class="icofont-facebook"></i></a>
            <a target="_blank" href="<?php echo $items['instagram']; ?>" class="instagram"><i class="icofont-instagram"></i></a>
            <a target="_blank" href="<?php echo $items['skype']; ?>" class="skype"><i class="icofont-skype"></i></a>
            <a target="_blank" href="<?php echo $items['linkedin']; ?>" class="linkedin"><i class="icofont-linkedin"></i></i></a>
        </div>
    </div>
</section>
