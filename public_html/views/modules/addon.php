<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
		<h3 class="page-title">Addon Modules</h3>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width:30px;padding-left:15px;">ID</th>
                                    <th>Module</th>
                                    <th class="text-right" style="width:130px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($modules as $i => $module) { ?>
                                    <?php /*
                                    [id] => 1 (2, 3, 4 etc)
                                    [module] => vanilla
                                    [title] => Vanilla
                                    [summary] => The Vanilla Module Summary
                                    [theme] => adminTheme
                                    [icon] => lnr lnr-leaf
                                    [sort] => 0 (1, 2, 3, 4 etc)
                                    [visibility] => 1 or 0
                                    [status] => -1 or 0 or 1
                                    [system] => 0 or 1
                                    */ ?>
                                <tr>
                                    <td style="width:30px;padding-left:15px;"><i class="<?php echo $module['icon']; ?>"></i></td>
                                    <td>
                                        <h5 style="margin:0;"><?php echo $module['title']; ?></h5>
                                        <small class="hide-below-640"><?php echo $module['summary']; ?></small>
                                    </td>
                                    <td class="text-right" style="width:200px;">
                                        <?php if ($module['status'] == 1) { ?>
                                        <a href="/admin/modules/settings/<?php echo $module['module']; ?>">
                                            <span class="label label-primary">Settings</span></a>
                                        <a href="/admin/modules/disable/<?php echo $module['module']; ?>">
                                            <span class="label label-danger">Disable</span></a>
                                        <?php } else if ($module['status'] == 0) { ?>
                                        <a href="/admin/modules/enable/<?php echo $module['module']; ?>">
                                            <span class="label label-success">Enable</span></a>
                                        <a href="/admin/modules/uninstall/<?php echo $module['module']; ?>">
                                            <span class="label label-warning">Uninstall</span></a>
                                        <?php } else if ($module['status'] == -1) { ?>
                                        <a href="/admin/modules/install/<?php echo $module['module']; ?>">
                                            <span class="label label-primary">Install</span></a>
                                        <a href="/admin/modules/delete/<?php echo $module['module']; ?>">
                                            <span class="label label-danger">Delete</span></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
