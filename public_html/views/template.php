<!doctype html>
<html>
<head>
    <title><?php echo $this->template->title->default("Default title"); ?></title>
    <?php echo $this->template->meta; ?>
    <?php echo $this->template->stylesheet; ?>
</head>
<body>
    <?php
        // This is an example to show that you can load stuff from inside the template file
        echo $this->template->widget('navigation', array('title' => $this->config->item('siteName', 'settings')));
    ?>
    <div class="container" style="margin-top: 60px;">
        <?php
            // This is the main content partial
            echo $this->template->content;
        ?>
        <hr>
        <footer>
            <p>
                <?php
                    // Show the footer partial, and prepend copyright message
                    echo $this->template->footer->prepend('&copy; ' . date("Y") . ' ELYK Innovation, Inc.');
                ?>
            </p>
        </footer>
    </div>
</body>
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<?php echo $this->template->javascript; ?>
</html>