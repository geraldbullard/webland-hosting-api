<div data-type="container" data-preview="/views/snippets/preview/eterna_text.png" data-keditor-title="Thumbnail Panel" data-keditor-categories="Eterna;Image">
    <div class="row">
        <div class="col-sm-12">
            <div class="thumbnail" data-type="container-content">
                <div data-type="component-photo">
                    <div class="photo-panel">
                        <img src="/assets/vendor/kedit/snippets/img/somewhere_bangladesh.jpg" width="100%" height="" class="img-responsive" />
                    </div>
                </div>
                <div data-type="component-text">
                    <h3>Thumbnail label</h3>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p>
                        <a href="#" class="btn btn-primary" role="button">Button</a>
                        <a href="#" class="btn btn-default" role="button">Button</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
