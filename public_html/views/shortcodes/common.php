<?php
/* Common Shortcodes */

// Site URL
function siteUrl() {
    $CI =& get_instance(); // Always include the CodeIgniter Instance
    return 'http://ci/';
}
Shortcode::add('siteUrl', 'siteUrl');

// Contact Us Form
function contactUsForm() {
    $CI =& get_instance(); // Always include the CodeIgniter Instance
    $success = false;
    ob_start();
    if (isset($_POST['verb']) && $_POST['verb'] == 'contact') {
        $CI->load->library('form_validation');
        $CI->form_validation->set_rules('name', 'Name', 'trim|required');
        $CI->form_validation->set_rules('email', 'Email', 'trim|required');
        $CI->form_validation->set_rules('subject', 'Subject', 'required');
        $CI->form_validation->set_rules('message', 'Message', 'trim|required');
        if ($CI->form_validation->run() === FALSE) {
            //die("Failed Validation");
        } else {
            $success = true;
        }
    }
    if ($success) {
        // Handle $_POST, show TY
        ?>
        <strong>THANK YOU!</strong>
        <p>Your message has been submitted. Someone will contact you shortly.</p>
        <?php
    } else {
        ?>
        <!-- Google (Invisible/Badge) reCAPTCHA v2 -->
        <script src="https://www.google.com/recaptcha/api.js"></script>
        <script>
            // After successful return submit the form
            function onSubmit(token) {
                document.getElementById("contact_us_form").submit();
            }
        </script>
        <?php
        // Open the <form> tag
        echo form_open_multipart('', array('id' => 'contact_us_form'));
        echo form_hidden('verb', 'contact');
        // Text
        echo '<div class="form-group">';
        echo form_label('Name:', 'name', array('class' => 'control-label'));
        echo form_error('name', '<div class="alert alert-danger" role="alert">', '</div>');
        echo form_input('name', $CI->input->post('name'), array('class' => 'form-control', 'placeholder' => 'Full Name'));
        echo '</div>';
        // Email
        echo '<div class="form-group">';
        echo form_label('Email:', 'email', array('class' => 'control-label'));
        echo form_error('email', '<div class="alert alert-danger" role="alert">', '</div>');
        echo form_input('email', $CI->input->post('email'), array('class' => 'form-control', 'placeholder' => 'Email'));
        echo '</div>';
        // Select
        echo '<div class="form-group">';
        echo form_label('Subject:', 'subject', array('class' => 'control-label'));
        echo form_error('subject', '<div class="alert alert-danger" role="alert">', '</div>');
        echo form_dropdown('subject', array(
            '' => 'Select',
            'Feedback' => 'Feedback',
            'Report a Bug' => 'Report a Bug',
            'Feature Request' => 'Feature Request',
            'More Information' => 'More Information',
        ), $CI->input->post('subject'), array('class' => 'form-control'));
        echo '</div>';
        // Textarea
        echo '<div class="form-group">';
        echo form_label('Message:', 'message', array('class' => 'control-label'));
        echo form_error('message', '<div class="alert alert-danger" role="alert">', '</div>');
        echo form_textarea(array(
            'name' => 'message',
            'class' => 'form-control',
            'placeholder' => 'Message',
            'rows' => '3'
        ), $CI->input->post('message'));
        echo '</div>';
        // Submit w Google reCAPCTHA
        echo '<div class="form-group">';
        // Bind the Google reCPATCHA to the submit button
        echo form_submit('send', 'Send', array(
            'class' => 'btn btn-info g-recaptcha',
            'data-sitekey' => '6LdlIu8ZAAAAAKmnBZTo3R6DlVlL7Xl5bZUvVNCE',
            'data-callback' => 'onSubmit',
            'data-action' => 'submit'
        ));
        echo '</div>';
        // Close the </form> tag
        echo form_close();
    }
    $output_string = ob_get_contents();
    ob_end_clean();

    return $output_string;
}
Shortcode::add('contactUsForm', 'contactUsForm');

// Google reCAPTCHA, setup later for non Form Builder Forms
/*function googleReCaptcha() {
    $CI =& get_instance(); // Always include the CodeIgniter Instance
    return '<div><p>Google reCAPTCHA</p></div>';
}
Shortcode::add('googleReCaptcha', 'googleReCaptcha');*/