<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->template->title->default("Default title"); ?></title>
    <?php echo $this->template->meta; ?>
    <link href="/assets/eterna/img/favicon.png" rel="icon">
    <link href="/assets/eterna/img/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/eterna/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="/assets/vendor/font-awesome/css/fa-all-5.13.0.css" rel="stylesheet">
    <link href="/assets/eterna/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/eterna/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="/assets/eterna/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/assets/eterna/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="/assets/eterna/css/style.css" rel="stylesheet">
    <link href="/assets/eterna/css/mdb-pro.scss" rel="stylesheet">
    <link href="/assets/eterna/css/compiled-addons.min.css" rel="stylesheet">
    <link href="/assets/eterna/css/bp_custom.css" rel="stylesheet">
    <?php echo $this->template->stylesheet; ?>
</head>
<body>
    <?php echo $this->template->widget('eterna/topbar'); ?>
    <header id="header">
        <div class="container d-flex">
            <div class="logo mr-auto">
                <h1 class="text-light"><a href="/"><span>brokenPIXEL</span></a></h1>
                <!-- <a href="index.html"><img src="/assets/eterna/img/logo.png" alt="" class="img-fluid"></a>-->
            </div>
            <?php echo $this->template->widget('eterna/primarynav'); ?>
        </div>
    </header>
    <?php echo $this->template->content; ?>
    <?php echo $this->template->widget('eterna/fullfooter'); ?>
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
    <script src="/assets/eterna/vendor/jquery/jquery.min.js"></script>
    <script src="/assets/eterna/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/eterna/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="/assets/eterna/vendor/php-email-form/validate.js"></script>
    <script src="/assets/eterna/vendor/jquery-sticky/jquery.sticky.js"></script>
    <script src="/assets/eterna/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="/assets/eterna/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="/assets/eterna/vendor/counterup/counterup.min.js"></script>
    <script src="/assets/eterna/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="/assets/eterna/vendor/venobox/venobox.min.js"></script>
    <script src="/assets/eterna/js/main.js"></script>
    <?php echo $this->template->javascript; ?>
</body>
</html>