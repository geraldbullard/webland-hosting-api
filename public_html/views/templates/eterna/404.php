<?php
/** Name: 404 */
?>
<section id="main">
    <div id="maincontent" class="container">
        <div class="row">
            <div id="fullcontent" class="col-lg-12 full-content">
                <h1>Well Now...</h1>
                <p>Sorry, we couldn't find that page/content you were looking for.</p>
            </div>
        </div>
    </div>
</section>
