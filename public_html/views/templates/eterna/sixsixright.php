<?php
/** Name: 2 Column (6-6, Content on Right) */
?>
<section id="main">
    <div id="maincontent" class="container">
        <div class="row">
            <div id="leftcontent" class="col-lg-6 left-content">
                <h3>Left Content</h3>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
            </div>
            <div id="rightcontent" class="col-lg-6 right-content">
                <h1 class="page-heading"><?php echo $pagedata->title; ?></h1>
                <?php echo $pagedata->content; ?>
            </div>
        </div>
    </div>
</section>
