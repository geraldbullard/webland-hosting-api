<?php
/** Name: Stock Home Page */
?>
    <?php echo $this->template->widget('eterna/hero'); ?>
    <main id="main">
        <?php echo $this->template->widget('eterna/featured'); ?>
        <?php echo $this->template->widget('eterna/about'); ?>
        <?php echo $this->template->widget('eterna/services'); ?>
        <?php echo $this->template->widget('eterna/clients'); ?>
    </main>
