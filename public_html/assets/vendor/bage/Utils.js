/** USEFUL FRONTEND QUERY FUNCTIONALITY
 * Last updated by Daniel Bage
 * On 03/15/2017
 *
 * Last method added was swapChildElements
 *
 * METHODS
 * addClassToLiWithExternalLinks
 * addClassToLiWithPdfs
 * addHvrClass
 * addOrRemoveStackClass
 * clearContentFromFooter
 * clearContentFromHeader
 * documentReady
 * documentScrolled
 * elementDistanceFromViewportrtbs
 * externalLinksTargetBlank
 * fadeIn
 * fadeContentOnLink
 * fixHeader
 * fixFooter
 * freeFooter
 * isDevice
 * getQueryVariable
 * matchHeights
 * matchHeightsOfEqualVerticalPosition
 * matchHeightsOfSecondElement
 * maximiseContentArea
 * print_r
 * removeEmptyElements
 * responsiveTables
 * restoreHeights
 * stackCells
 * swapChildElements
 * swapElements
 * tallestHeight
 * viewportLoaded
 * viewportOrientationChanged
 * viewportResized
 * viewportResizedFinished
 * viewportWidth
 * formatPhone
 * copyTextToClipboard
 *
 */

var Utils =
    {
        /**
         * Enum for device names.
         * @readonly
         * @enum {string}
         */
        device: {
            IPHONE5_PORTRAIT: 'IPHONE5 PORTRAIT',
            GALAXY_PORTRAIT: 'GALAXY PORTRAIT',
            IPHONE6_PORTRAIT: 'IPHONE6 PORTRAIT',
            CELL_PORTRAIT: 'CELL PORTRAIT',
            IPHONE4_LANDSCAPE: 'IPHONE4 LANDSCAPE',
            IPHONE5_LANDSCAPE: 'IPHONE5 LANDSCAPE',
            GALAXY_LANDSCAPE: 'GALAXY LANDSCAPE',
            IPHONE6_LANDSCAPE: 'IPHONE6 LANDSCAPE',
            CELL: 'CELL',
            TABLET_PORTRAIT: 'TABLET PORTRAIT',
            TABLET: 'TABLET',
            DESKTOP: 'DESKTOP'
        },
        /**
         * @description Add .pdf to li elements that contain links to PDFS
         */
        addClassToLiWithPdfs: function () {
            jQuery('[href$= ".pdf"]').each(function () {
                jQuery(this).parent('li').addClass('contains_pdf');
            });
        },
        /**
         * @description Add .external_link to li elements that contain links to external web pages
         */
        addClassToLiWithExternalLinks: function () {
            jQuery('.external_link').each(function () {
                jQuery(this).parent('li').addClass('contains_external_link');
            });
        },
        addHvrClass: function (hoverType, elemString) {
            /*hoverTypes:
             grow, shrink,
             pulse, pulse-grow, pulse-shrink,
             push, pop,
             bounce-in, bounce-out,
             rotate, grow-rotate,
             float, sink, bob, hang,
             skew, skew-forward, slew-backward,
             wobble-horizontal, wobble-vertical, wobble-to-bottom-right, wobble-to-top-right, wobble-top, wobble-bottom, wobble-skew
             buzz, buzz-out

             see http://ianlunn.github.io/Hover/ for more
             */
            jQuery(elemString).each(function () {
                jQuery(this).addClass('hvr-' + hoverType).css('cursor', 'pointer');
            });
        },
        /**
         * @description Add padding to top of content area, equal to header height
         * @param {string} [headerElem = '#headerwrap'] Header area element
         * @param {string} [contentElem = '#contentwrap'] Content area element
         */
        clearContentFromHeader: function (headerElem, contentElem) {
            //defaults
            var header = jQuery('#headerwrap');
            var content = jQuery('#middlewrap');

            //overwrite if provided
            if (typeof headerElem !== 'undefined') {
                header = jQuery(headerElem);
            }
            if (typeof contentElem !== 'undefined') {
                content = jQuery(contentElem);
            }

            //add css
            content.css('padding-top', header.outerHeight());
        },
        /**
         * @description Add padding to bottom of content area, equal to footer height
         * @param {string} [contentElem = '#middlewrap'] Content area element
         * @param {string} [footerElem = '#footerwrap'] Footer area element
         */
        clearContentFromFooter: function (contentElem, footerElem) {
            //defaults
            var content = jQuery('#middlewrap');
            var footer = jQuery('#footerwrap');

            //overwrite if provided
            if (typeof contentElem !== 'undefined') {
                content = jQuery(contentElem);
            }
            if (typeof footerElem !== 'undefined') {
                footer = jQuery(footerElem);
            }

            //add css
            content.css('padding-bottom', footer.outerHeight());
        },
        /**
         * @description Once document is ready, perform callback
         * @param {function} [callback] Callback for document ready
         */
        documentReady: function (callback) {
            jQuery(document).ready(callback);
        },
        /**
         * @description Add 'scrolled' class to body when an element is a specified distance from the top of the viewport
         * @param {int} [triggerDistance = -100] The distance the element is from the viewport top.
         * @param {string} [triggerElement = #middlewrap] The element that acts as a trigger.
         */
        documentScrolled: function (triggerDistance, triggerElement) {
            if (!triggerDistance) {
                triggerDistance = -65;
            }
            if (!triggerElement) {
                triggerElement = '#main';
            }
            // adding scrolled class
            var headerClear = true;
            var body = jQuery('body');
            var timeout;

            jQuery(window).scroll(function () {
                    clearTimeout(timeout);

                    var headerY = jQuery(triggerElement).offset().top;
                    var scroll = headerY - jQuery(window).scrollTop();
                    //console.log(scroll);
                    if ((scroll < triggerDistance) && headerClear) {
                        body.toggleClass('scrolled');
                        headerClear = false;
                    }
                    else if ((scroll > triggerDistance) && !headerClear) {
                        body.toggleClass('scrolled');
                        headerClear = true;
                    }
                    timeout = setTimeout(function () {
                        // do your stuff
                    }, 500);
                }
            )
        },
        /**
         * @description Once viewport is loaded, perform callback
         * @param {function} [callback] Callback for window load
         */
        viewportLoaded: function (callback) {
            jQuery(window).load(callback);
        },
        /**
         * @description Once orientation is changed, perform callback
         * @param {function} [callback] Callback for orientation changed
         */
        viewportOrientationChanged: function (callback) {
            window.addEventListener("orientationchange", callback, false);
        },
        /**
         * @description As viewport is re-sized, perform callback
         * @param {function} [callback] Callback for viewport resizing
         */
        viewportResized: function (callback) {
            jQuery(window).resize(callback);
        },
        /**
         * @description Once viewport resizing ends, perform callback
         * @param {function} [callback] Callback for viewport resizing ending
         */
        viewportResizedFinished: function (callback) {
            var resizeTimer;
            jQuery(window).on('resize', function (e) {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function () {
                    callback();
                }, 250);
            });
        },
        /**
         * @description Pixel-less viewport width
         * @returns {number} Viewport width
         */
        viewportWidth: function () {
            var viewportWidth = jQuery(window).width();
            return viewportWidth;
        },
        /**
         * @description Set heights equally on source elements
         * @param {string|JQuery} source The source elements to match heights of
         * @param {boolean} [includeMargin = false] Whether to include element margins
         */
        matchHeights: function (source, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }

            for (var i = 0; i < list.length; i++) {
                var src = jQuery(list[i]);
                var tgtHt = 0;
                src.each(function () {
                    if ((jQuery(this).outerHeight(includeMargin)) > tgtHt) {
                        tgtHt = jQuery(this).outerHeight(includeMargin);

                    }
                });
                jQuery(src).css('height', (tgtHt));
            }
        },
        /**
         * @description Return pixel-less value of tallest source element
         * @param {string|JQuery} source The source elements to compare heights of
         * @param {boolean} [includeMargin = false] Whether to include element margins
         * @returns {number} Pixel-less height
         */
        tallestHeight: function (source, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }

            for (var i = 0; i < list.length; i++) {
                var src = jQuery(list[i]);
                var tgtHt = 0;
                src.each(function () {
                    if ((jQuery(this).outerHeight(includeMargin)) > tgtHt) {
                        tgtHt = jQuery(this).outerHeight(includeMargin);

                    }
                });
                return tgtHt;
            }
        },
        /**
         * @description Remove all css heights from source elements
         * @param {string|JQuery} source The source elements to remove css heights of
         */
        restoreHeights: function (source) {
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }
            for (var i = 0; i < list.length; i++) {
                jQuery(list[i]).css("height", "");
            }
        },
        /**
         * @description Set heights equally on source elements with same vertical position
         * @param {string|JQuery} source The source elements to match
         * @param {boolean} [includeMargin = false] Whether to include element margins
         */
        matchHeightsOfEqualVerticalPosition: function (source, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }
            for (var i = 0; i < list.length; i++) {
                var src = jQuery(list[i]);

                var elementsWithEqualVerticalPositions = [];
                var positionOfFirstElement = jQuery(src[0]).position();
                var verticalPositionOfFirstElement = 0;
                if (positionOfFirstElement) {
                    verticalPositionOfFirstElement = positionOfFirstElement.top;
                }
                var y = 0;
                jQuery.each(src, function () {

                    var srcP = jQuery(this).position();
                    var srcY = srcP.top;
                    //console.log(srcY);
                    if (srcY == y) {
                        jQuery(this).addClass('match');
                    }
                    else {

                        Utils.matchHeights('.match', includeMargin);
                        srcP = jQuery(this).position();
                        srcY = srcP.top;
                        y = srcY;
                        jQuery('.match').removeClass('match');
                        jQuery(this).addClass('match');
                    }
                });

                Utils.matchHeights('.match', includeMargin);
                jQuery('.match').removeClass('match');
            }
        },
        /**
         * @description Make height of element A match element B
         * @param {string} elementA The element to receive new height
         * @param {string} elementB The element to match height of
         * @param {boolean} [includeMargin = false] Whether to include the elementA margin
         */
        matchHeightsOfSecondElement: function (elementA, elementB, includeMargin) {
            if (typeof(includeMargin) === 'undefined') {
                includeMargin = false;
            }
            var heightOfSecondSource = jQuery(elementB).outerHeight(includeMargin);
            jQuery(elementA).css('height', heightOfSecondSource);
        },
        /**
         * @description Move element B infront of element A
         * @param {string} elemA First Element
         * @param {string} elemB Second element to be moved in front of first
         */
        swapElements: function (elemA, elemB) {
            jQuery(elemA).before(jQuery(elemB));
        },
        /**
         * @description Determine device based on viewport width
         * @param {Utils.device} deviceName Device name
         * @returns {boolean}
         */
        isDevice: function (deviceName) {
            deviceName = deviceName.toUpperCase();
            var width = jQuery(window).width();
            if ((width < 321) && deviceName == Utils.device.IPHONE5_PORTRAIT) {
                return true
            }
            else if ((width < 361) && deviceName == Utils.device.GALAXY_PORTRAIT) {
                return true
            }
            else if ((width < 376) && deviceName == Utils.device.IPHONE6_PORTRAIT) {
                return true
            }
            else if ((width < 481) && ((deviceName == Utils.device.CELL_PORTRAIT) || (deviceName == Utils.device.IPHONE4_LANDSCAPE))) {
                return true
            }
            else if ((width < 569) && deviceName == Utils.device.IPHONE5_LANDSCAPE) {
                return true
            }
            else if ((width < 641) && ((deviceName == Utils.device.GALAXY_LANDSCAPE))) {
                return true
            }
            else if ((width < 667) && ((deviceName == Utils.device.IPHONE6_LANDSCAPE))) {
                return true
            }
            else if ((width < 768) && ((deviceName == Utils.device.CELL))) {
                return true
            }
            else if ((width < 769) && (width > 640) && deviceName == Utils.device.TABLET_PORTRAIT) {
                return true
            }
            else if ((width < 1025) && (width > 736) && deviceName == Utils.device.TABLET) {
                return true
            }
            else if ((width > 1024) && deviceName == Utils.device.DESKTOP) {
                return true
            }
            else {
                return false
            }
        },
        /**
         * @description Once viewport finishes loading, fade in source element
         * @param {string|JQuery} source Source element to fade in
         * @param {number} [delay = 0] Delay before fade time in milliseconds
         * @param {number} [fadeInTime = 0] Fade duration in milliseconds
         */
        fadeIn: function (source, delay, fadeInTime) {
            var list = [];
            if (jQuery.isArray(source)) {
                list = source;
            }
            else {
                list.push(source);
            }
            for (var i = 0; i < list.length; i++) {
                jQuery(list[i]).each(function (index) {
                    var item = jQuery(this);
                    item.css('opacity', 0);
                    Utils.viewportLoaded(function () {
                        item.delay(delay * index).animate({
                            opacity: 1
                        }, fadeInTime);
                    });
                });
            }
        },
        /**
         * @description content area fades out on navigation click
         * @param {string} [triggerElement = .primary-menu li a] Elements that trigger the fade
         * @param {string} [mainContentContainer = #middlewrap] Container element to be faded
         */
        fadeContentOnLink: function (triggerElement, mainContentContainer) {
            if (!triggerElement) {
                triggerElement = 'a';
            }
            if (!mainContentContainer) {
                mainContentContainer = '#middlewrap';
            }
            jQuery(triggerElement).each(function () {
                jQuery(this).click(function () {
                    jQuery(mainContentContainer).animate({
                        opacity: 0
                    });
                });
            })
        },
        getQueryVariable: function (variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return (false);
        },
        addOrRemoveStackClass: function (commaSeperatedStringOfSelectors) {
            var cellsToStack = jQuery(commaSeperatedStringOfSelectors);
            cellsToStack.each(function () {
                if (Utils.isDevice('IPHONE6 LANDSCAPE')) {
                    jQuery(this).addClass('stack');
                } else {
                    jQuery(this).removeClass('stack');
                }
            })
        },
        stackCells: function (commaSeperatedStringOfSelectors) {
            if (typeof commaSeperatedStringOfSelectors === 'undefined') {
                commaSeperatedStringOfSelectors = '.cell';
            }
            Utils.documentReady(function () {
                Utils.addOrRemoveStackClass(commaSeperatedStringOfSelectors);
            });
            Utils.viewportOrientationChanged(function () {
                Utils.addOrRemoveStackClass(commaSeperatedStringOfSelectors);
            });
            Utils.viewportResized(function () {
                Utils.addOrRemoveStackClass(commaSeperatedStringOfSelectors);
            });
        },
        elementDistanceFromViewport: function (elem, side) {
            var scroll = jQuery(window).scrollTop();
            var elementOffset = jQuery(elem).parent().offset().top;
            var windowHeight = jQuery(window).height();
            var distance = '';

            switch (side) {
                case 'right':

                    break;
                case 'bottom':
                    elementOffset = (jQuery(elem).offset().top);
                    distance = Math.abs((windowHeight - elementOffset) - scroll);
                    break;
                case 'left':

                    break;
                default:
                    distance = (elementOffset - scroll);
            }
            return distance
        },
        /**
         * @description Fix the header to the top of the viewport
         * @param {string} [header_elem = '#headerwrap']
         */
        fixHeader: function (header_elem) {
            //defaults
            var header = jQuery('#headerwrap');

            //overwrite if provided
            if (typeof header_elem !== 'undefined') {
                header = jQuery(header_elem);
            }

            //set top based on logged in or not
            var top = jQuery('#wpadminbar').outerHeight();

            //add css
            header.css({
                'position': 'fixed',
                'top': top,
                'width': '100%',
                'z-index': '3000',
                'box-shadow': '0 0 10px #000000'
            });

            //clear content from header
            this.clearContentFromHeader(header_elem);

            var anchors = jQuery('.anchor');
            var headerHeight = header.outerHeight();
            var adminBarHeight = jQuery('#wpadminbar').outerHeight();
            if (jQuery('body').hasClass('logged-in')) {
                headerHeight += adminBarHeight;
            }
            ;
            anchors.each(function () {
                jQuery(this).css('top', -headerHeight);
            });
        }
        ,
        /**
         * @description Fix the footer to the bottom of the viewport
         * @param {string} [footer_elem = '#footerwrap']
         */
        fixFooter: function (footer_elem) {
            //defaults
            var footer = jQuery('#footerwrap');

            //overwrite if provided
            if (typeof footer_elem !== 'undefined') {
                footer = jQuery(footer_elem);
            }

            //add css
            footer.css({
                'position': 'fixed',
                'bottom': '0',
                'width': '100%',
                'z-index': '1'
            });

            //clear content from footer
            this.clearContentFromFooter();
        },
        /**
         * @description Remove footer css styling
         * @param {string} [footer_elem = '#footerwrap'] Footer area element
         */
        freeFooter: function (footer_elem) {
            //defaults
            var footer = jQuery('#footerwrap');

            //overwrite if provided
            if (typeof footer_elem !== 'undefined') {
                footer = jQuery(footer_elem);
            }

            //remove css
            footer.removeAttr('style');
        },
        /**
         * @description Maximise content area so it at least fills the viewport
         * @param {string} [headerElem = '#header'] Header element
         * @param {string} [contentElem = '#middlewrap'] Content element
         * @param {string} [footerElem = '#footerwrap'] Footer element
         */
        maximiseContentArea: function (headerElem, contentElem, footerElem) {
            //defaults
            var header = jQuery('#headerwrap');
            var content = jQuery('#middlewrap');
            var footer = jQuery('#footerwrap');
            var adminBar = jQuery('#wpadminbar');

            //overwrite if provided
            if (typeof headerElem !== 'undefined') {
                header = jQuery(headerElem);
            }
            if (typeof contentElem !== 'undefined') {
                content = jQuery(contentElem);
            }
            if (typeof footerElem !== 'undefined') {
                footer = jQuery(footerElem);
            }

            //additional vars
            var windowHeight = jQuery(window).height();
            var headerHeight = header.outerHeight();
            var footerHeight = footer.outerHeight();
            var adminBarHeight = adminBar.outerHeight();

            if ((header).css('position') == 'fixed') {
                headerHeight = 0;
            }

            if ((footer).css('position') == 'fixed') {
                footerHeight = 0;
            }

            //add css
            content.css('min-height', windowHeight - headerHeight - footerHeight - adminBarHeight);
        },
        /**
         * @description Adds target="_blank" to all links that leave the current domain
         */
        externalLinksTargetBlank: function () {
            //get the domain
            var domain = location.hostname + (location.port ? ':' + location.port : '');
            var setTarget = true;
            var target = null;

            //loop through each link element that doesn't already have a target
            jQuery('a:not([target])').each(function (index, element) {
                setTarget = false;
                target = null;

                //if the link is javascript, don't mess with it
                if (element.href.indexOf('javascript:') != -1) {
                    // break out, go to next element
                    return true;
                }

                // href doesn't contain our domain
                if (element.href.indexOf(domain) == -1) {
                    target = "_blank";
                    setTarget = true;
                }

                // link opens a pdf, Word doc, csv, or txt file
                if (jQuery.inArray(
                        element.href.substr(element.href.lastIndexOf('.') + 1),
                        ['pdf', 'doc', 'docx', 'csv', 'txt']) > -1) {
                    target = "_blank";
                    setTarget = true;
                }

                // set the element's target
                if (setTarget !== false) {
                    element.target = target;
                    jQuery(element).addClass('external_link');
                }

                return true;
            });
            this.addClassToLiWithExternalLinks();
        },
        /**
         * @description Remove empty elements
         * @param {string} [element = p] The element type to remove.
         */
        removeEmptyElements: function (element) {
            if (!element) {
                element = 'p';
            }
            jQuery(element).each(function () {
                var elem = jQuery(this);
                if (elem.html().replace(/\s|&nbsp;/g, '').length == 0)
                    elem.remove();
            });
        },
        /**
         * @description PHP-like print_r() & var_dump() equivalent for JavaScript Object
         * @author Faisalman - movedpixel@gmail.com
         * @license http://www.opensource.org/licenses/mit-license.php
         * @link http://gist.github.com/879208
         *
         * examples:
         *   // alert a boolean value
         *   alert(Utils.isDevice("TABLET"));
         *   // alert an object/array
         *   alert(Utils.print_r(Utils.device));
         */
        print_r: function (obj) {
            // define tab spacing
            var tab = '';
            // check if it's array
            var isArr = Object.prototype.toString.call(obj) === '[object Array]' ? true : false;
            // use {} for object, [] for array
            var str = isArr ? ('Array [' + tab + '\n') : ('Object {' + tab + '\n');
            // walk through it's properties
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    var val1 = obj[prop];
                    var val2 = '';
                    var type = Object.prototype.toString.call(val1);
                    switch (type) {
                        // recursive if object/array
                        case '[object Array]':
                        case '[object Object]':
                            val2 = print_r(val1, (tab + '\t'));
                            break;
                        case '[object String]':
                            val2 = '\'' + val1 + '\'';
                            break;
                        default:
                            val2 = val1;
                    }
                    str += tab + '\t' + prop + ' => ' + val2 + ',\n';
                }
            }
            // remove extra comma for last property
            str = str.substring(0, str.length - 2) + '\n' + tab;
            return isArr ? (str + ']') : (str + '}');
            var var_dump = print_r; // equivalent function
        },
        /**
         * @description Move element B infront of element A inside many children elements
         * @param {string} childElem Child Element
         * @param {string} elemA First Element
         * @param {string} elemB Second element to be moved in front of first
         */
        swapChildElements: function (childElem, elemA, elemB) {
            var children = jQuery(childElem);
            children.each(function () {
                var child = jQuery(this);
                var a = child.find(elemA);
                var b = child.find(elemB);
                a.before(b);
            })
        },
        /**
         * @description Make generic HTML tables with theads, responsive
         */
        responsiveTables: function () {
            var headertext = [];
            var headers = document.querySelectorAll("thead");
            var tablebody = document.querySelectorAll("tbody");

            for (var i = 0; i < headers.length; i++) {
                headertext[i] = [];
                for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) {
                    var current = headrow;
                    headertext[i].push(current.textContent.replace(/\r?\n|\r/, ""));
                }
            }
            if (headers.length > 0) {
                for (var h = 0, tbody; tbody = tablebody[h]; h++) {
                    for (var i = 0, row; row = tbody.rows[i]; i++) {
                        for (var j = 0, col; col = row.cells[j]; j++) {
                            col.setAttribute("data-th", headertext[h][j]);
                        }
                    }
                }
            }
        },
        /**
         *
         */
        fixDevURL: function(){
            //in order for this to work the script must be localised and a [theme] object with a [site_url] property passed into it
            if(theme.site_url) {
                var site_url = theme.site_url;
                //var internalLinks = jQuery("a[href^='"+siteURL+"'], a[href^='/'], a[href^='./'], a[href^='../'], a[href^='#']");
                //only interested in links starting with /
                var internalLinks = jQuery("a[href^='/']");
                internalLinks.each(function () {
                    var url = jQuery(this).attr('href');
                    jQuery(this).attr('href', site_url + url);

                });
            }
        },
        /**
         * reformat phone numbers to (111) 222-3333
         */
        formatPhone: function(phoneNumberString) {
            var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
            var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
            if (match) {
                return '(' + match[1] + ') ' + match[2] + '-' + match[3];
            }
            return phoneNumberString;
        },
        /**
         * reformat phone numbers to (111) 222-3333
         */
        cleanPhone: function(phoneNumberString) {
            var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
            return cleaned;
        },

        copyTextToClipboard: function(text, element) {
            var textArea = document.createElement("textarea");

            //
            // *** This styling is an extra step which is likely not required. ***
            //
            // Why is it here? To ensure:
            // 1. the element is able to have focus and selection.
            // 2. if element was to flash render it has minimal visual impact.
            // 3. less flakyness with selection and copying which **might** occur if
            //    the textarea element is not visible.
            //
            // The likelihood is the element won't even render, not even a flash,
            // so some of these are just precautions. However in IE the element
            // is visible whilst the popup box asking the user for permission for
            // the web page to copy to the clipboard.
            //

            // Place in top-left corner of screen regardless of scroll position.
            textArea.style.position = 'fixed';
            textArea.style.top = 0;
            textArea.style.left = 0;

            // Ensure it has a small width and height. Setting to 1px / 1em
            // doesn't work as this gives a negative w/h on some browsers.
            textArea.style.width = '2em';
            textArea.style.height = '2em';

            // We don't need padding, reducing the size if it does flash render.
            textArea.style.padding = 0;

            // Clean up any borders.
            textArea.style.border = 'none';
            textArea.style.outline = 'none';
            textArea.style.boxShadow = 'none';

            // Avoid flash of white box if rendered for any reason.
            textArea.style.background = 'transparent';


            textArea.value = text;

            document.body.appendChild(textArea);

            textArea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

            document.body.removeChild(textArea);

            if(!!element) {
                element = $(element).find('.sprite-copy');
                $(element).fadeOut(100, function() {
                    $(this)
                        .addClass('sprite-accept')
                        .removeClass('sprite-copy')
                        .fadeIn(500)
                        .delay(500)
                        .fadeOut(100, function() {
                        $(this)
                            .addClass('sprite-copy')
                            .removeClass('sprite-accept')
                            .fadeIn(500);
                    });
                });
            }
        }
    }
;
