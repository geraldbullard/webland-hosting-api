import initPreviewAction from './initPreviewAction';
import initFullscreenAction from './initFullscreenAction';
// No Save functionality needed for brokenPIXEL
//import initSaveAction from './initSaveAction';

export default function () {
    let self = this;
    let options = self.options;
    
    initPreviewAction.apply(self);
    initFullscreenAction.apply(self);
    // No Save functionality needed for brokenPIXEL
    //typeof options.onSave === 'function' && initSaveAction.apply(self);
};
