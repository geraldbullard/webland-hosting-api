<?php
/*
 * Demo widget
 */

class Navigation extends Widget
{

    public function display($data)
    {

        if (!isset($data['items'])) {
            $data['items'] = array(
                array('title' => 'Admin', 'url' => 'admin/dashboard/'),
                array('title' => 'Dashboard', 'url' => 'dashboard/'),
                //array('title' => 'Home', 'url' => ''),
                //array('title' => 'About', 'url' => 'about/'),
                //array('title' => 'Contact', 'url' => 'contact/'),
                array(
                    'title' => ($this->session->userdata('user_id') ? 'Logout' : 'Login'),
                    'url' => ($this->session->userdata('user_id') ? 'user/logout/' : 'user/login/')
                ),
            );
        }

        $this->view('widgets/navigation', $data);
    }

}
