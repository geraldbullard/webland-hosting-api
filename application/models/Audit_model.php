<?php

class Audit_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function addAuditLog($type, $url, $msg, $module, $rel_id) {
		//Set the Audit Log info
		$auditLogInfo = array(
			'audit_types_id' => $type,
			'users_id' => $this->session->userdata('user_id'),
			'url' => $url,
			'remote_host' => $_SERVER['REMOTE_ADDR'],
			'datetime' => date("Y-m-d H:i:s"),
			'user_agent' => $_SERVER['HTTP_USER_AGENT'],
			'message' => $msg,
			'module' => $module,
			'related_id' => $rel_id,
		);

		//Insert the logged action into the database
		$this->db->insert($this->db->dbprefix . "audit", $auditLogInfo);
	}

	// Filter Settings
	public function filter($filter)
	{
		$this->db->distinct();

		if ($this->session->userdata('group') > 2) $this->db->where('users_id', $this->session->userdata('user_id'));
		$this->db->order_by("id", "desc");
		$audits = $this->db->get($this->db->dbprefix . "audit");

		if ($audits->num_rows() < 1) {
			return false;
		}
		return $audits->result_array();
	}

}
