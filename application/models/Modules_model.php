<?php

class Modules_model extends CI_Model
{
    
    public $all_modules, $installed_modules;

	public function __construct()
	{
		parent::__construct();
        
        // Get all modules and create arrays
        foreach (self::moduleInfo(null, true) as $module) {
            $this->all_modules[] = $module['module'];
            if ($module['status'] !== "-1") $this->installed_modules[] = $module['module'];
        }

        // Get module folders and compare for any uninstalled modules and load init file
        $module_folders = array_diff(scandir(APPPATH . 'modules'), array('.', '..', 'index.html'));
        foreach ($module_folders as $module) {
            if (!in_array($module, $this->all_modules)) {
                if (is_file(APPPATH . 'modules/' . $module . '/init.php')) {
                    include(APPPATH . 'modules/' . $module . '/init.php');
                    bp_initializeModule($this->db);
                }
            }
        }
	}

	// Module Info
	public function moduleInfo($module = null, $all = false)
	{
		if ($module) $this->db->where("module", "$module");
        if (!$all) $this->db->where("status != ", -1);
		$this->db->order_by("sort ASC, title ASC");
		$module_info = $this->db->get($this->db->dbprefix . "modules");

        if ($module_info->num_rows() < 1) {
			return false;
		}
        if ($module) return $module_info->first_row();
        return $module_info->result_array();
	}

	// Module Info
	public function modulePermissions($module = null, $groups_id = null)
	{
		$this->db->where("module", "$module");
		if ($groups_id) $this->db->where("groups_id", $groups_id);
		$module_info = $this->db->get($this->db->dbprefix . "permissions");

        if ($module_info->num_rows() < 1) {
			return false;
		}
        if ($groups_id) return $module_info->first_row();
        return $module_info->result_array();
	}

	// Module Settings
	public function settings($module)
	{
		$this->db->distinct();

		$this->db->where("type", "$module");
		$this->db->order_by("id", "asc");
		$module_settings = $this->db->get($this->db->dbprefix . "settings");

		if ($module_settings->num_rows() < 1) {
			return false;
		}
		return $module_settings->result_array();
	}

	// Module Setting
	public function editsetting($settingid)
	{
		$this->db->where("id", "$settingid");
		$module_setting = $this->db->get($this->db->dbprefix . "settings");

		if ($module_setting->num_rows() < 1) {
			return false;
		}
		return $module_setting->result_array();
	}

	// Enable Module
	public function enable($module)
	{
		$this->db->set('status', 1);
		$this->db->where("module", "$module");
		if ($this->db->update($this->db->dbprefix . "modules")) return true;
        return false;
	}

	// Disable Module
	public function disable($module)
	{
        $this->db->set('status', 0);
        $this->db->where("module", "$module");
        if ($this->db->update($this->db->dbprefix . "modules")) return true;
        return false;
	}

	// Install Module
	public function install()
	{
		$module = $this->uri->segment(4);
        // Include the modules init file and do the work
        require(APPPATH . 'modules/' . $module . '/functions.php');
        if (bp_installModule()) return true;
        return false;
	}

	// Uninstall Module
	public function uninstall($module)
	{
        // Include the modules init file and do the work
        require(APPPATH . 'modules/' . $module . '/functions.php');
        if (bp_uninstallModule()) return true;
        return false;
	}

	// Delete Module
	public function delete($module)
	{
        $this->load->helper('file');
        delete_files(APPPATH . 'modules/' . $module, TRUE);
        rmdir(APPPATH . 'modules/' . $module);
        $this->db->where("module", "$module");
        $this->db->delete($this->db->dbprefix . "modules");
        $this->db->where("module", "$module");
        $this->db->delete($this->db->dbprefix . "permissions");
        return true;
	}

}
