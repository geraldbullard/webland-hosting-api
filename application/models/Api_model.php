<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* API Methods /////////////////////////////////////////////////////////////////
    [POST] /auth/login json { "username" : "admin", "password" : "Admin123$"}
    [GET] /book
    [POST] /book/create json { "title" : "x", "author" : "xx"}
    [PUT] /book/update/:id json { "title" : "y", "author" : "yy"}
    [GET] /book/detail/:id
    [DELETE] /book/delete/:id
    [POST] /auth/logout
*///////////////////////////////////////////////////////////////////////////////

/* API Example Call ////////////////////////////////////////////////////////////
    $url = "https://api.weblandhosting.com/auth/login";
    $params = array(
        'username' => '',
        'password' => '',
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Client-Service: wlh-api-client',
        'Auth-Key: wlhapiauthkey',
        'Content-Type: application/x-www-form-urlencoded'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($response);
    $verified = false;
    if ($result->status == '200') {
        // Later we will check for the correct product that matches
        $verified = true;
    }
*///////////////////////////////////////////////////////////////////////////////

class Api_model extends CI_Model {

    var $client_service = "wlh-api-client";
    var $auth_key = "wlhapiauthkey";

    public function check_auth_client()
    {
        $client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key  = $this->input->get_request_header('Auth-Key', TRUE);

        if ($client_service == $this->client_service && $auth_key == $this->auth_key) {
            return true;
        } else {
            return json_output(401, array('status' => 401,'message' => 'Unauthorized'));
        }
    }

    public function login($username, $password)
    {
        $q = $this->db->select('password, id')->from($this->db->dbprefix . 'apiusers')->where('username', $username)->get()->row();

        if ($q == "") {
            return array('status' => 204,'message' => 'Username not found');
        } else {
            $hashed_password = $q->password;
            $id = $q->id;
            //echo $hashed_password . " " . $password;
            //exit();
            if (hash_equals($hashed_password, crypt($password, $hashed_password))) {
                // Thinking of getting any product specific HTML here
                $premium_content_html = '<div id="premium_content_wrapper" class="premium-content-wrapper">Premium Content</div>';
                // First let's empty the authentication table of any existing tokens
                $this->db->trans_start();
                $this->db->where('users_id', $id);
                $this->db->delete($this->db->dbprefix . 'apiusers_authentication');
                $this->db->trans_commit();
                // Now do the work
                $last_login = date('Y-m-d H:i:s');
                $token = crypt(substr(md5(rand()), 0, 7), $this->config->config['encryption_key']);
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('id', $id)->update($this->db->dbprefix . 'apiusers', array('last_login' => $last_login));
                $this->db->insert($this->db->dbprefix . 'apiusers_authentication', array('users_id' => $id, 'token' => $token, 'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('status' => 500, 'message' => 'Internal server error');
                } else {
                    $this->db->trans_commit();
                    return array(
                        'status' => 200,
                        'message' => 'Successfully logged in',
                        'id' => $id,
                        'token' => $token,
                        'premium_html' => $premium_content_html
                    );
                }
            } else {
                echo "Wrong password";
                exit();
                return array('status' => 204, 'message' => 'Wrong password');
            }
        }
    }

    public function logout()
    {
        $users_id = $this->input->get_request_header('User-ID', TRUE);
        $token = $this->input->get_request_header('Token', TRUE);
        $this->db->where('users_id', $users_id)->where('token', $token)->delete($this->db->dbprefix . 'apiusers_authentication');
        return array('status' => 200, 'message' => 'Successfully logged out');
    }

    public function auth()
    {
        $users_id = $this->input->get_request_header('User-ID', TRUE);
        $token = $this->input->get_request_header('Token', TRUE);
        $q = $this->db->select('expired_at')->from($this->db->dbprefix . 'apiusers_authentication')->where('users_id', $users_id)->where('token', $token)->get()->row();
        if ($q == "") {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized'));
        } else {
            if ($q->expired_at < date('Y-m-d H:i:s')) {
                return json_output(401, array('status' => 401, 'message' => 'Your session has expired'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id', $users_id)->where('token', $token)->update($this->db->dbprefix . 'apiusers_authentication', array('expired_at' => $expired_at, 'updated_at' => $updated_at));
                return array('status' => 200, 'message' => 'Authorized');
            }
        }
    }

    public function product_details($id)
    {
        return $this->db->select('*')->from($this->db->dbprefix . 'products')->where('id', $id)->get()->row();
    }

    public function all_products()
    {
        return $this->db->select('*')->from($this->db->dbprefix . 'products')->order_by('title', 'asc')->get()->result();
    }

}
