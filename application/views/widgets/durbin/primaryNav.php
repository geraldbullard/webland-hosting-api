<?php

class PrimaryNav extends Widget
{

    public function display($data)
	{
        $contents_model = $this->load->model('contents/contents_model');
        $data['menu'] = $this->contents_model->getMenuByDesignation('primary');
        /*if ($this->session->userdata('user_id')) {
            $_menuData = unserialize($data['menu']->menuData);
            // Admin Dashboard
            $_menuData['item'][] = 'no-parent';
            $_menuData['title'][] = 'Admin';
            $_menuData['slug'][] = 'admin/dashboard/';
            $_menuData['class'][] = 'a-class';
            $_menuData['target'][] = '_self';
            $data['menu']->menuData = serialize($_menuData);
        }*/
        $this->view('widgets/durbin/primarynav', $data);
    }
    
}