<?php

class Topbar extends Widget
{

    public function display($data)
	{
        $data['items'] = array(
            // get from db settings later
            'email' => $this->config->item('adminEmail', 'settings'),
            'phone' => $this->config->item('sitePhone', 'settings'),
            'twitter' => 'https://www.twitter.com/',
            'facebook' => 'https://www.facebook.com/',
            'instagram' => 'https://www.instagram.com/',
            'skype' => 'https://www.skype.com/',
            'linkedin' => 'https://www.linkedin.com/',
        );
        $this->view('widgets/durbin/topbar', $data);
    }
    
}