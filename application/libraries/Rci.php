<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rci
{
    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->library('session');
        
        if (!$CI->session->userdata('rci_Data')) {
            $rci_Data = array(
                'rci_Data' => array(
                    'folders' => array()
                )
            );
            $CI->session->set_userdata($rci_Data);
        }
    }

    public static function get($pageName, $function, $display = true)
    {
        $CI =& get_instance();

        $pageName = strtolower($pageName);
        $function = strtolower($function);
        $rci_holder = '';

        if (!array_key_exists($pageName, $CI->session->userdata('rci_Data')['folders'])) {
            self::_build_folder($pageName);
        }

        if (array_key_exists($function, $CI->session->userdata('rci_Data')['folders'][$pageName])) {
            foreach ($CI->session->userdata('rci_Data')['folders'][$pageName][$function] as $f => $fileName) {
                // safety check in case a fasle positive was received on the session check
                if (!file_exists(APPPATH . 'hooks/rci/' . $pageName . '/' . $fileName)) {
                    unset($CI->session->userdata('rci_Data')['folders'][$pageName]);
                    continue;
                }
                $rci = '';
                if ($pageName == 'stylesheet') {  // special case of a style sheet
                    $rci = '<link rel="stylesheet" type="text/css" href="' . APPPATH . 'hooks/rci/' . $pageName . '/' . $fileName . '">';
                } else {
                    include(APPPATH . 'hooks/rci/' . $pageName . '/' . $fileName);
                }
                if ($display == true) {
                    $rci_holder .= '<!-- RCI [BOM] -' . $pageName . '-' . $function . ' : ' . APPPATH . 'hooks/rci/' .
                        $pageName . '/' . $fileName . ' -->' . "\n" . $rci . '<!-- RCI [EOM] -' . $pageName . '-' .
                        $function . ' : ' . APPPATH . 'hooks/rci/' . $pageName . '/' . $fileName . ' -->' . "\n";
                } else {
                    $rci_holder .= $rci;
                }
            }
        }
        return $rci_holder;
    }

    public static function _build_folder($pageName)
    {
        $CI =& get_instance();

        $rci_Data = array(
            'rci_Data' => array(
                'folders' => array(
                    $pageName => array()
                )
            )
        );
        $CI->session->set_userdata($rci_Data);

        if (is_dir(APPPATH . 'hooks/rci/' . $pageName)) {
            $filesFound = array();
            if ($pageName == 'stylesheet') {
                $pattern = '/(\w*)_*(\w+)_(\w+)_(\w+)\.css$/';
            } else {
                $pattern = '/(\w*)_*(\w+)_(\w+)_(\w+)\.php$/';
            }
            $dir = opendir(APPPATH . 'hooks/rci/' . $pageName);
            while ($file = readdir($dir)) {
                if ($file == '.' || $file == '..') continue;
                $match = array();
                if (preg_match($pattern, $file, $match) > 0) {
                    if ($match[3] == $pageName) {
                        $filesFound[$match[0]] = $match[4];
                    }
                }
            }
            if (count($filesFound) > 0) {
                ksort($filesFound);
                $this_Data = array();
                foreach ($filesFound as $file => $function) {
                    $this_Data['rci_Data']['folders'][$pageName][$function][] = $file;
                }
                $CI->session->set_userdata($this_Data);
            }
        }
    }

}
