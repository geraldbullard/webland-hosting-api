<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* apiusers Routes */
$route['admin/apiusers'] = 'apiusers/admin_index';
$route['admin/apiusers/add'] = 'apiusers/add';
$route['admin/apiusers/getapiusers'] = 'apiusers/getapiusers';
$route['admin/apiusers/edit/(:any)'] = 'apiusers/edit/$1';
$route['admin/apiusers/update/(:any)'] = 'apiusers/update/$1';
$route['admin/apiusers/delete/(:any)'] = 'apiusers/delete/$1';
