<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * API User Model
 */
class Apiusers_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	// Filter API Users
	public function filter($filter)
	{
		$this->db->distinct();

		/* Some examples of how wqe build the sql query based on filter data
		if (isset($filter->id) && $filter->id !== null) {
			$this->db->where("E.id", $filter->id);
		}
		if (isset($filter->first_name) && $filter->first_name !== null) {
			$this->db->where("first_name", $filter->first_name);
		}
		if (isset($filter->state[0]) && !empty($filter->state[0])) {
			$where = array();
			foreach ($filter->state as $state) {
				$where[] = "ELI.resident_state = '" . $state . "' OR ENRLI.non_resident_state = '" . $state . "'";
			}
			$where_str = implode(" OR ", $where);
			$this->db->where($where_str);
		}*/

		// $this->db->select('U.*');
		$apiusers = $this->db->get($this->db->dbprefix . "apiusers");
		// $this->data['apiusers'] = $apiusers->result_array();
		// $this->db->join('enrollers_experience EE', 'E.id = EE.id_enroller', 'left');
		// $this->db->group_by('E.id');
		/*echo '<pre>';
		print_r($this->db->last_query());
		die();*/

		if ($apiusers->num_rows() < 1) {
			return false;
		}
		return $apiusers->result_array();
	}

	function getapiuser($id)
	{
		$this->db->where("id", $id);
		return $this->db->get($this->db->dbprefix . "apiusers")->first_row();
	}

	function add()
	{
		return true;
	}

	function update()
	{
		return true;
	}

	function delete($id)
	{
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "apiusers")) return true;
		return false;
	}

}
