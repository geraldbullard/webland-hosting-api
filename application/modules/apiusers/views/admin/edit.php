<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/apiusers">API Users</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="panel">
            <div class="panel-body">
                <form action="" method="post" id="apiuser_edit">
                    <p>
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="<?php echo $apiuser->name; ?>">
                    </p>
                    <p class="text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </p>
                </form>
            </div>
		</div>
