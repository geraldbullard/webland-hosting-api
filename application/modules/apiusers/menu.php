<?php
/* API Users module menu file */

$_menu = array(
    'url' => '/admin/apiusers/',
    'title' => 'API Users',
    'icon' => 'lnr lnr-users',
    'access' => 1,
    /*'sub_items' => array(
    	// Repeat main menu since it has subs
	    'apiusers' => array(
		    'url' => '/admin/apiusers/',
		    'title' => 'API Users',
		    'icon' => 'lnr lnr-users',
		    'access' => 1,
	    ),
	    'add' => array(
		    'url' => '/admin/apiusers/add/',
		    'title' => 'Add API User',
		    'icon' => 'lnr lnr-plus-circle',
		    'access' => 3,
	    ),
    ),*/
);