<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	function __construct()
	{
        parent::__construct();

        // Make sure the user is logged in for this module
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	public function admin_index()
	{
        $this->data = array();
        $this->data['title'] = 'Dashboard';
        // Added for developer dropdown
        $this->data['dev_data'] = '';

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('dashboard/admin/index', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
	}

	public function tracking() 
	{
        $this->data = array();
        $this->data['title'] = 'Tracking';
        
        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('dashboard/admin/tracking', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

}
