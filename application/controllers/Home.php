<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	public function index()
	{
		$this->data = array();
		$this->data['title'] = "Home";

		$this->load->view('layout/bootstrap_header.php', $this->data);
		$this->load->view('home/index', $this->data);
		$this->load->view('layout/bootstrap_footer.php', $this->data);
	}
}
