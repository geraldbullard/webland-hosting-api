<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

        // Make sure the user is logged in for this module
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	public function index()
	{
        $this->data = array();
        $this->data['title'] = "Audit";

        // if not a developer or admin group, show only users related data
        if ($this->session->userdata('group') > 2) $this->db->where('users_id', $this->session->userdata('user_id'));

        $this->db->order_by('id', 'DESC'); // newest first
        $audits = $this->db->get($this->db->dbprefix . "audit");
        $this->data['audits'] = $audits->result_array();

        // Added for developer dropdown
        $this->data['dev_data'] = $audits->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('audit/index', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function getAudits()
    {
        $filter = new stdClass();
        //$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

        $this->load->model('Audit_model');
        $data['audits'] = $this->Audit_model->filter($filter);

        return json_encode($this->load->view('audit/filter', $data));
    }

}
