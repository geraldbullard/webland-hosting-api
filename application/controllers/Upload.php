<?php

class Upload extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        // Make sure the user is logged in for this module
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        // Get site wide settings first
        $this->Settings_model->loadSitewideSettings();
        // Load the form helper
        $this->load->helper(array('form', 'url'));
    }

    public function do_upload()
    {
        if ($this->input->post('userid') !== false) $userid = $this->input->post('userid');
        $config['upload_path'] = './bp_uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 6000;
        $config['max_width'] = 1920;
        $config['max_height'] = 1080;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile'))
        {
            // Set the error to the flashdata array:
            $this->session->set_flashdata(array("msg" => "Error uploading the file!" . "\n\n" . $this->upload->display_errors(), "msgtype" => "danger"));
        }
        else
        {
            if ($this->input->post('userid') !== false) {
                $data = $this->upload->data();
                $data['userid'] = $userid;
                if ($this->db->insert($this->db->dbprefix . "users_files", $data)) {
                    // Set the success message to the flashdata array:
                    $this->session->set_flashdata(
                        array("msg" => 'Successfully uploaded the User Image!' . "\n\n" .
                        '<a target="_blank" href="http://ci/bp_uploads/' . $this->upload->data()['file_name'] . '">' .
                        $this->upload->data()['raw_name'] . '</a>', 'msgtype' => 'success')
                    );
                } else {
                    // Set the error to the flashdata array:
                    $this->session->set_flashdata(
                        array("msg" => "Error inserting the User Image data into the database!", "msgtype" => "danger")
                    );
                }
            } else {
                // Set the success message to the flashdata array:
                $this->session->set_flashdata(
                    array("msg" => "Successfully uploaded the file!", "msgtype" => "success")
                );
            }
        }
        redirect($this->input->post('redirect'), 'refresh');
    }
    
}