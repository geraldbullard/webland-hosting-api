<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	public function login()
	{
		$this->data = array();
		$this->data['title'] = "Login";

		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('form');			
			$this->load->view('layout/admin/minimal-header.php', $this->data);
			$this->load->view('users/login.php', $this->data);
			$this->load->view('layout/admin/minimal-footer.php', $this->data);
		}
		else
		{
			$remember = (bool)$this->input->post('remember');
			if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember))
			{
				// first we see if there is a page they wished to go to
				if ($this->session->userdata('page_url')) {
					redirect($this->session->userdata('page_url'));
					// if not we send them to the welcome/dashboard
				} else {
					//Load the Audit Model and Insert a log entry of this action
					$this->load->model('Audit_model');
					$this->Audit_model->addAuditLog(
						$this->config->config['settings']['auditTypeUserLogin'],
						'/user/login',
						'User Logged In',
						'user',
						$this->session->userdata('user_id')
					);
					redirect('/admin/dashboard/', 'refresh');
					// Later we redirect based on user types or config setting per user acct etc...
					//redirect('dashboard');
				}
			}
			else
			{
			    $_SESSION['auth_message'] = $this->ion_auth->errors();
				$this->session->mark_as_flash('auth_message');
				redirect('user/login');
			}
		}
	}

	public function logout()
	{
		//Load the Audit Model and Insert a log entry of this action
		$this->load->model('Audit_model');
		$this->Audit_model->addAuditLog(
			$this->config->config['settings']['auditTypeUserLogout'],
			'/user/logout',
			'User Logged Out',
			'user',
			$this->session->userdata('user_id')
		);
		$this->ion_auth->logout();
		redirect('/');
	}

}
