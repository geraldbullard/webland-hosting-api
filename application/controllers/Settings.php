<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

        // Make sure the user is logged in for this module
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	public function index()
	{
		$this->data = array();
		$this->data['title'] = "Settings";

		// hide settings associated with core modules
		$this->db->where("type != ", "dashboard");
		$this->db->where("type != ", "users");
		$this->db->where("type != ", "roles");
		$this->db->where("type != ", "settings");
		$this->db->where("type != ", "audit");
		// hide settings associated with core modules
		$this->db->order_by("id", "asc");
		$settings = $this->db->get($this->db->dbprefix . "settings");
		$this->data['settings'] = $settings->result_array();
		// Added for developer dropdown
		$this->data['dev_data'] = $settings->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('settings/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function getSettings()
	{
		$filter = new stdClass();
		//$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

		$this->load->model('Settings_model');
		$data['settings'] = $this->Settings_model->filter($filter);

		return json_encode($this->load->view('settings/filter', $data));
	}

	public function edit()
	{
		$settingid = $this->uri->segment(4);

		$this->data = array();

		// START - The Edit Stuff //////////////////////////////////////////////////////////////////////////////////////
		if ($this->input->post('define') != '') {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
			$this->form_validation->set_rules('define', 'Setting Define', 'trim|required');
			if ($this->form_validation->run() !== FALSE)
			{
				// Set the Setting information !!!!! (define is not changeable, delete and re-add for now) !!!!!
				// any other columns in the groups table not id or name, add any new columns to this array
				$settinginfo = array(
					'define' => $this->input->post('define'),
					'title' => $this->input->post('title'),
					'summary' => $this->input->post('summary'),
					'value' => $this->input->post('value'),
					'type' => $this->input->post('type')
				);

				// If it doesn't update:
				if (!$this->db->update($this->db->dbprefix . "settings", $settinginfo, ['id' => $settingid]))
				{
					// Set the error to the data array:
					$this->data['msg'] = "Error Updating the Setting Information!";
					$this->data['msgtype'] = "danger";
				}
				// If we get here, it means the process was successful.
				else
				{
					// Add Audit Trail
					$this->load->model('Audit_model');
					$this->Audit_model->addAuditLog(
						$this->config->config['settings']['auditTypeRecordUpdated'],
						'/settings/edit',
						'Setting Information Updated',
						'setting',
						$settingid
					);

					// Set the success message:
					$this->data['msg'] = "Setting Information Successfully Updated!";
					$this->data['msgtype'] = "success";
				}
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors('<div>', '</div>');
				$this->data['msgtype'] = "danger";
			}
		}
		// END - The Edit Stuff ////////////////////////////////////////////////////////////////////////////////////////

		$this->data['title'] = "Edit Setting";

		$setting = $this->db->where("id", $settingid);
		$setting = $this->db->get($this->db->dbprefix . "settings");
		$this->data['setting'] = $setting->result_array();

		// Added for developer dropdown
		$this->data['dev_data'] = $setting->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('settings/edit', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function add()
	{
		$this->data = array();

		// START - The Edit Stuff //////////////////////////////////////////////////////////////////////////////////////
		if ($this->input->post('define') != '') {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
			$this->form_validation->set_rules('define', 'Setting Define', 'trim|required|is_unique[ci_settings.define]');
			$this->form_validation->set_message('is_unique', '%s already exists.');
			$this->form_validation->set_rules('title', 'Setting Title', 'trim|required');
			$this->form_validation->set_rules('summary', 'Setting Summary', 'trim|required');
			$this->form_validation->set_rules('value', 'Setting Value', 'trim|required');
			$this->form_validation->set_rules('type', 'Setting Type', 'required');
			$this->form_validation->set_rules('edit', 'Setting Edit Capability', 'required');
			$this->form_validation->set_rules('system', 'Setting System Designation', 'required');
			$this->form_validation->set_rules('visibility', 'Setting Visibility', 'required');
			if ($this->form_validation->run() !== FALSE)
			{
				// Set the insert info from $_POST values
                $settinginfo = array(
                    'define' => $this->input->post('define'),
                    'title' => $this->input->post('title'),
                    'summary' => $this->input->post('summary'),
                    'value' => $this->input->post('value'),
                    'type' => $this->input->post('type'),
                    'edit' => $this->input->post('edit'),
                    'system' => $this->input->post('system'),
                    'visibility' => $this->input->post('visibility')
                );

                // Insert the Setting information
                $settingid = $this->db->insert($this->db->dbprefix . "settings", $settinginfo);

				// If it doesn't update:
				if (!$settingid)
				{
					// Set the error to the data array:
					$this->data['msg'] = "Error Creating the Setting!";
					$this->data['msgtype'] = "danger";
				}
				// If we get here, it means the process was successful.
				else
				{
					// Add Audit Trail
					$this->load->model('Audit_model');
					$this->Audit_model->addAuditLog(
						$this->config->config['settings']['auditTypeRecordCreated'],
						'/settings/add',
						'New Setting Created',
						'settings',
						$settingid
					);

					// Set the success message:
					$this->data['msg'] = "Setting Created Successfully!";
					$this->data['msgtype'] = "success";
				}
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors('<div>', '</div>');
				$this->data['msgtype'] = "danger";
			}
		}
		// END - The Edit Stuff ////////////////////////////////////////////////////////////////////////////////////////

		$this->data['title'] = "Create Setting";

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('settings/add', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function delete()
	{
		$settingid = $this->uri->segment(4);
		if (!$this->Settings_model->delete($settingid))
		{
            // Set the success message to the flashdata array:
            $this->session->set_flashdata(array("msg" => "Error Deleting the Setting!", "msgtype" => "danger"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordDeleted'],
				'/settings/delete',
				'Setting Deleted',
				'settings',
                $settingid
			);
            // Set the success message to the flashdata array:
            $this->session->set_flashdata(array("msg" => "Setting Successfully Deleted!", "msgtype" => "success"));
		}
		redirect('/settings/', 'refresh');
	}

}
