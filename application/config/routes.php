<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user/login'; // change to siteIndex db setting later
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* API */
$route['auth/login']['post'] = 'auth/login';
$route['auth/logout']['post'] = 'auth/logout';
$route['book']['get'] = 'book';
$route['book/detail/(:num)']['get'] = 'book/detail/$1';
$route['book/create']['post'] = 'book/create';
$route['book/update/(:num)']['put'] = 'book/update/$1';
$route['book/delete/(:num)']['delete'] = 'book/delete/$1';

/* Dahboard */
$route['admin/dashboard'] = 'dashboard/admin_index';

/* Users */
$route['admin/users'] = 'users/admin_index';
$route['admin/users/add'] = 'users/add';
$route['admin/users/getUsers'] = 'users/getUsers';
$route['admin/users/edit/(:any)'] = 'users/edit/$1';
$route['admin/users/update/(:any)'] = 'users/update/$1';
$route['admin/users/delete/(:any)'] = 'users/delete/$1';
$route['admin/users/activate/(:any)'] = 'users/activate/$1';
$route['admin/users/deactivate/(:any)'] = 'users/deactivate/$1';
$route['admin/users/unlock/(:any)'] = 'users/unlock/$1';

/* Roles */
$route['admin/roles'] = 'roles/index';
$route['admin/roles/add'] = 'roles/add';
$route['admin/roles/getRoles'] = 'roles/getRoles';
$route['admin/roles/edit/(:any)'] = 'roles/edit/$1';
$route['admin/roles/update/(:any)'] = 'roles/update/$1';
$route['admin/roles/delete/(:any)'] = 'roles/delete/$1';
$route['admin/roles/activate/(:any)'] = 'roles/activate/$1';
$route['admin/roles/deactivate/(:any)'] = 'roles/deactivate/$1';

/* Settings */
$route['admin/settings'] = 'settings/index';
$route['admin/settings/add'] = 'settings/add';
$route['admin/settings/getSettings'] = 'settings/getSettings';
$route['admin/settings/edit/(:any)'] = 'settings/edit/$1';
$route['admin/settings/update/(:any)'] = 'settings/update/$1';
$route['admin/settings/delete/(:any)'] = 'settings/delete/$1';

/* Modules */
$route['admin/modules'] = 'modules/index';
$route['admin/modules/addon'] = 'modules/addon';
$route['admin/modules/rci'] = 'modules/rci';
$route['admin/modules/add'] = 'modules/add';
$route['admin/modules/getModules'] = 'modules/getModules';
$route['admin/modules/edit/(:any)'] = 'modules/edit/$1';
$route['admin/modules/update/(:any)'] = 'modules/update/$1';
$route['admin/modules/delete/(:any)'] = 'modules/delete/$1';
$route['admin/modules/enable/(:any)'] = 'modules/enable/$1';
$route['admin/modules/disable/(:any)'] = 'modules/disable/$1';
$route['admin/modules/settings/(:any)'] = 'modules/settings/$1';
$route['admin/modules/editsetting/(:any)'] = 'modules/editsetting/$1';
$route['admin/modules/install/(:any)'] = 'modules/install/$1';
$route['admin/modules/uninstall/(:any)'] = 'modules/uninstall/$1';

/* Audit */
$route['admin/audit'] = 'audit/index';
$route['admin/audit/getAudits'] = 'audit/getAudits';

$modules_path = APPPATH . 'modules/';
$modules = scandir($modules_path);
foreach($modules as $module)
{
    if ($module === '.' || $module === '..') continue;
    if (is_dir($modules_path) . '/' . $module)
    {
        $routes_path = $modules_path . $module . '/config/routes.php';
        if (file_exists($routes_path))
        {
            require($routes_path);
        }
        else
        {
            continue;
        }
    }
}
