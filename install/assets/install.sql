-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 29, 2019 at 02:28 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_audit`
--

DROP TABLE IF EXISTS `ci_audit`;
CREATE TABLE IF NOT EXISTS `ci_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_types_id` int(11) DEFAULT NULL,
  `users_id` int(10) UNSIGNED DEFAULT NULL,
  `url` varchar(511) DEFAULT NULL,
  `remote_host` char(15) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `message` longtext,
  `module` varchar(64) DEFAULT NULL,
  `related_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_audit_types_id` (`audit_types_id`),
  KEY `idx_audit_users_id` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_audit`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_groups`
--

DROP TABLE IF EXISTS `ci_groups`;
CREATE TABLE IF NOT EXISTS `ci_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_groups`
--

INSERT INTO `ci_groups` (`id`, `name`, `description`) VALUES
(1, 'developer', 'Developer'),
(2, 'admin', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `ci_login_attempts`
--

DROP TABLE IF EXISTS `ci_login_attempts`;
CREATE TABLE IF NOT EXISTS `ci_login_attempts` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_modules`
--

DROP TABLE IF EXISTS `ci_modules`;
CREATE TABLE IF NOT EXISTS `ci_modules` (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(255) NOT NULL,
  `icon` varchar(64) NOT NULL,
  `sort` int(3) NOT NULL,
  `visibility` int(1) NOT NULL DEFAULT '1',
  `status` int(1) NOT NULL DEFAULT '1',
  `system` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_modules_moduled` (`module`),
  KEY `idx_modules_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_modules`
--

INSERT INTO `ci_modules` (`id`, `module`, `title`, `summary`, `theme`, `icon`, `sort`, `visibility`, `status`, `system`) VALUES
(1, 'dashboard', 'Dashboard', 'The Dashboard Module Summary', 'adminTheme', 'lnr lnr-screen', 0, 1, 1, 1),
(2, 'users', 'Users', 'The Users Module Summary', 'adminTheme', 'lnr lnr-users', 1, 1, 1, 1),
(3, 'roles', 'Roles', 'The Roles Module Summary', 'adminTheme', 'lnr lnr-chart-bars', 2, 1, 1, 1),
(4, 'settings', 'Settings', 'The Settings Module Summary', 'adminTheme', 'lnr lnr-cog', 3, 1, 1, 1),
(5, 'modules', 'Modules', 'The Modules Module Summary', 'adminTheme', 'lnr lnr-layers', 4, 1, 1, 1),
(6, 'audit', 'Audit', 'The Audit Module Summary', 'adminTheme', 'lnr lnr-alarm', 5, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_permissions`
--

DROP TABLE IF EXISTS `ci_permissions`;
CREATE TABLE IF NOT EXISTS `ci_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groups_id` int(3) NOT NULL,
  `module` varchar(63) NOT NULL,
  `access` int(1) DEFAULT NULL,
  `override` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_access_roles_idx` (`groups_id`),
  KEY `ci_access_module_idx` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_permissions`
--

INSERT INTO `ci_permissions` (`id`, `groups_id`, `module`, `access`, `override`) VALUES
(1, 1, 'dashboard', 5, NULL),
(2, 1, 'users', 5, NULL),
(3, 1, 'roles', 5, NULL),
(4, 1, 'settings', 5, NULL),
(5, 1, 'modules', 5, NULL),
(6, 1, 'audit', 5, NULL),
(7, 2, 'dashboard', 4, NULL),
(8, 2, 'users', 4, NULL),
(9, 2, 'roles', 4, NULL),
(10, 2, 'settings', 4, NULL),
(11, 2, 'modules', 4, NULL),
(12, 2, 'audit', 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_settings`
--

DROP TABLE IF EXISTS `ci_settings`;
CREATE TABLE IF NOT EXISTS `ci_settings` (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `define` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `edit` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `system` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `visibility` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`define`),
  KEY `idx_options_define` (`define`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_settings`
--

INSERT INTO `ci_settings` (`id`, `define`, `title`, `summary`, `value`, `type`, `edit`, `system`, `visibility`) VALUES
(1, 'siteName', 'Site Name', 'The name to be displayed on your site pages.', 'Broken Pixel CMS', 'core', 1, 1, 1),
(2, 'siteSlogan', 'Site Slogan', 'Site Slogan', 'A truly simple, yet powerful MVC Framework', 'seo', 1, 1, 1),
(3, 'siteTheme', 'Site Theme', 'This is the name of your site theme', 'himu', 'design', 0, 1, 1),
(4, 'adminTheme', 'Admin Theme', 'Admin Theme', 'adminlte2', 'design', 1, 1, 1),
(5, 'siteDescription', 'Site Description', 'This is a site wide description. It will also get used in the event that you do not enter any meta tag description for any of your pages.', 'This is my site description.', 'seo', 0, 1, 1),
(6, 'siteKeywords', 'Site Keywords', 'These are site wide keywords. They will also get used in the event that you do not enter any meta tag keywords for any of your pages.', 'these, are, my, site, keywords', 'seo', 0, 1, 1),
(7, 'sitemapPriority', 'Sitemap Priority', 'The Content Priority for the Sitemap Generation (0.0 - 1.0)', '0.7', 'seo', 1, 1, 1),
(8, 'sitemapChangeFrequency', 'Sitemap Change Frequency', 'The Content Change Frequency for the Sitemap Generation (always, hourly, daily, weekly, monthly, yearly, never)', 'daily', 'seo', 1, 1, 1),
(9, 'sessionExpire', 'Admin Session Timeout', 'The time in minutes that your admin session will last before timing out and forcing you to login again.', '30', 'core', 0, 1, 1),
(10, 'defaultLang', 'Default Admin Language', 'The default language code of the site admin', 'en', 'core', 1, 1, 1),
(11, 'allowRegistrations', 'Allow Public User Registrations (Non Admin)', 'This setting will allow website users to make their own account registrations without the need to login to the site admin.', '0', 'core', 1, 1, 1),
(12, 'defaultCharSet', 'Default Character Set', 'Default Character Set', 'utf-8', 'core', 1, 1, 1),
(13, 'defaultTimeZone', 'Default Time Zone', 'Default Date/Time Time Zone for the site.', 'America/New_York', 'core', 1, 1, 1),
(14, 'auditsAdminPagination', 'Admin Audit Listing Pagination', 'The number of items to show per page in the admin audit listing page.', '15', 'audit', 1, 1, 0),
(15, 'usersAdminPagination', 'Admin Users Listing Pagination', 'The number of items to show per page in the admin users listing page.', '15', 'users', 1, 1, 0),
(16, 'rolesAdminPagination', 'Admin Roles Listing Pagination', 'The number of items to show per page in the admin roles listing page.', '15', 'roles', 1, 1, 0),
(17, 'settingsAdminPagination', 'Admin Settings Listing Pagination', 'The number of items to show per page in the admin settings listing page.', '15', 'settings', 1, 1, 0),
(18, 'auditTypeUserLogin', 'Audit Type User Login', 'Audit Type User Login', '1', 'audit', 1, 1, 0),
(19, 'auditTypeUserLogout', 'Audit Type User Logout', 'Audit Type User Logout', '2', 'audit', 1, 1, 0),
(20, 'auditTypeRecordCreated', 'Audit Type Record Created', 'Audit Type Record Created', '3', 'audit', 1, 1, 0),
(21, 'auditTypeRecordUpdated', 'Audit Type Record Updated', 'Audit Type Record Updated', '4', 'audit', 1, 1, 0),
(22, 'auditTypeRecordDeleted', 'Audit Type Record Deleted', 'Audit Type Record Deleted', '5', 'audit', 1, 1, 0),
(23, 'auditTypeModuleInstalled', 'Audit Type Module Installed', 'Audit Type Module Installed', '6', 'audit', 1, 1, 0),
(24, 'auditTypeModuleEnabled', 'Audit Type Module Enabled', 'Audit Type Module Enabled', '7', 'audit', 1, 1, 0),
(25, 'auditTypeModuleDisabled', 'Audit Type Module Disabled', 'Audit Type Module Disabled', '8', 'audit', 1, 1, 0),
(26, 'auditTypeModuleUninstalled', 'Audit Type Module Uninstalled', 'Audit Type Module Uninstalled', '9', 'audit', 1, 1, 0),
(27, 'auditTypeModuleDeleted', 'Audit Type Module Deleted', 'Audit Type Module Deleted', '10', 'audit', 1, 1, 0),
(28, 'auditTypeUserUpdate', 'Audit Type User Update', 'Audit Type User Update', '11', 'audit', 1, 1, 0),
(29, 'auditTypeUserCreated', 'Audit Type User Created', 'Audit Type User Created', '12', 'audit', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

DROP TABLE IF EXISTS `ci_users`;
CREATE TABLE IF NOT EXISTS `ci_users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_email` (`email`),
  UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  UNIQUE KEY `uc_remember_selector` (`remember_selector`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'admin', '$2y$12$7HEP4DsUQ/wMmPywFL97A.DfRVzHLsCNwrw/d2kjtTXIJdquyLdn6', 'admin@admin.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1268889823, 1577583597, 1, 'Admin', 'Istrator', 'ADMIN', '1112223333');

-- --------------------------------------------------------

--
-- Table structure for table `ci_users_files`
--
DROP TABLE IF EXISTS `ci_users_files`;
CREATE TABLE IF NOT EXISTS `ci_users_files` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `file_type` varchar(64) NOT NULL DEFAULT '',
  `file_path` varchar(255) NOT NULL DEFAULT '',
  `full_path` varchar(255) NOT NULL DEFAULT '',
  `raw_name` varchar(255) NOT NULL DEFAULT '',
  `orig_name` varchar(255) NOT NULL DEFAULT '',
  `client_name` varchar(255) NOT NULL DEFAULT '',
  `file_ext` varchar(12) NOT NULL DEFAULT '',
  `file_size` varchar(12) NOT NULL DEFAULT '',
  `is_image` tinyint(3) UNSIGNED NOT NULL,
  `image_width` int(11) UNSIGNED NOT NULL,
  `image_height` int(11) UNSIGNED NOT NULL,
  `image_type` varchar(12) NOT NULL DEFAULT '',
  `image_size_str` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users_files`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_users_groups`
--

DROP TABLE IF EXISTS `ci_users_groups`;
CREATE TABLE IF NOT EXISTS `ci_users_groups` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users_groups`
--

INSERT INTO `ci_users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ci_users_groups`
--
ALTER TABLE `ci_users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `ci_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `ci_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
