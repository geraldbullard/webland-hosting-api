<?php
if (
    file_exists('../application/config/config.php') &&
    file_exists('../application/config/database.php')
){
    header('Location: /');
    exit(3);
}
error_reporting(E_ALL); //Setting this to E_ALL showed that that cause of not redirecting were few blank lines added in some php files.
$db_config_path = '../application/config/database.php';

// Only load the classes in case the user submitted the form
if ($_POST) {
    // Load the classes and create the new objects
    require_once('includes/core_class.php');
    require_once('includes/database_class.php');

    $core = new Core();
    $database = new Database();

    // Validate the post data
    if ($core->validate_post($_POST) == true) {
        // First create the database, then create tables, then write config file
        if ($database->create_database($_POST) == false) {
            $message = $core->show_message('error', "The database could not be created, please verify your settings.");
        } else if ($database->create_tables($_POST) == false) {
            $message = $core->show_message('error', "The database tables could not be created, please verify your settings.");
        } else if ($core->write_config($_POST) == false) {
            $message = $core->show_message('error', "The application/config/config.php file could not be written!");
        } else if ($core->write_db_config($_POST) == false) {
            $message = $core->show_message('error', "The application/config/database.php file could not be written!");
        }
        // If no errors, redirect to registration page
        if (!isset($message)) {
            $redir = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
            $redir .= "://" . $_SERVER['HTTP_HOST'];
            $redir .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
            $redir = str_replace('install/', '', $redir);
            header('Location: install.php?success=' . urlencode($redir));
        }
    } else {
        $message = $core->show_message('error', 'Not all fields have been filled in correctly. The host, username, password, and database name are required.');
    }
}
?>
<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
    <title>Installation | brokenPIXEL</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/vendor/linearicons/style.css" rel="stylesheet" />
    <link href="assets/vendor/toastr/toastr.min.css" rel="stylesheet" />
    <link href="assets/klorofil/css/main.css" rel="stylesheet" />
    <link href="assets/klorofil/css/bp_custom.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" />
    <link href="assets/klorofil/img/apple-icon.png" rel="apple-touch-icon" sizes="76x76" />
    <link href="assets/klorofil/img/favicon.png" rel="icon" sizes="96x96" type="image/png" />
</head>
<body>
    <div class="container installer">
        <h1>Install</h1>
        <div class="row">
            <div class="col-lg-3 col-md-1 col-sm-12"></div>
            <div class="col-lg-6 col-md-10 col-sm-12">
                <img class="img-responsive center-block" src="/assets/klorofil/img/bp-logo-bg.png" style="max-height:320px;" />
            </div>
            <div class="col-lg-3 col-md-1 col-sm-12"></div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php if (file_exists($db_config_path) && is_writable($db_config_path) || !file_exists($db_config_path)) { ?>
                <?php if (isset($_GET['success'])) { ?>
                <h2><strong>Installation Complete</strong></h2>
                <p><a href="/">View Site</a></p>
                <p><a href="/user/login">Login</a> U: admin | P: password<br><strong>Change Your Password Immediately!!!</strong></p>
                <?php } else { if (isset($message)) { echo '<p class="error">' . $message . '</p>'; } ?>
                <form id="install_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <h3>Database settings</h3>
                    <p>
                        <label for="hostname">Hostname</label>
                        <input type="text" id="hostname" name="hostname" value="localhost" placeholder="localhost" class="form-control" />
                    </p>
                    <p>
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" class="form-control" />
                    </p>
                    <p>
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" />
                    </p>
                    <p>
                        <label for="database">Database Name</label>
                        <input type="text" id="database" name="database" class="form-control" />
                    </p>
                    <p>
                        <input type="submit" value="Install" id="submit" class="form-control" />
                    </p>
                </form>
                <?php }
                } else { ?>
                <p class="error">Please make the application/config/database.php file writable.
                    <strong>Example</strong>:<br/><br/><code>chmod 777 application/config/database.php</code></p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
    <script src="assets/klorofil/vendor/jquery/jquery.min.js"></script>
    <script src="assets/klorofil/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/klorofil/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/klorofil/vendor/toastr/toastr.min.js"></script>
    <script src="assets/klorofil/scripts/klorofil-common.js"></script>
</body>
</html>
</html>